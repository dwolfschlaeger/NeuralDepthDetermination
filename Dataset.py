import os
import pickle
import logging

import numpy as np

import global_settings as gs
from utils.generic_utils import compute_statistics, hd_print2
from Log import init_logger

logger = logging.getLogger(gs.LOGGER_NAME)


class Dataset:
  """
  Provides a dataset which can either be a dataset which is initialized from a config file, a pickle file directly or a
  randomly generated dataset
  """

  def __init__(self):
    # params read from data
    self.N = None
    self.H = None
    self.W = None
    self.C = None
    self.X = None
    self.Y = None
    # self.Y_labels = None
    self.X_train = None
    self.Y_train = None
    # self.Y_train_labels = None
    self.X_test = None
    self.Y_test = None
    # self.Y_test_labels = None
    self.X_val = None
    self.Y_val = None
    # self.Y_val_labels = None
    self.specimen_names = None
    self.specimen_names_train = None
    self.specimen_names_test = None
    self.excitation_freqs = None
    # self.classes = None
    self.specimen_idx_to_name = None

    # params read from config
    self.test_split = 0
    self.val_split = 0
    self.dim_reduction = None
    self.input_shape = gs.INPUT_SHAPE
    self.data_path = None
    self.shuffle_specimens = None
    self.shuffle_pixels = False
    self.normalization = None
    self.loss = 'mae'
    self.class_to_thickness = None
    self.idx_to_class = None

    self.norm_function = None

  def __repr__(self):
    return self.shape_info()

  def __str__(self):
    return self.shape_info()

  def init_dataset_from_config(self, config):
    """
    Initializes dataset attributes from the config file, does splitting and normalization
    :param Config.Config config:
    :return: None
    """
    init_logger(config)
    logger.debug('Initializing dataset from config')
    self.data_path = config.value('data_path', None)
    data = self.load_data_from_file(self.data_path)
    # assert data.data_type == config.value('data_type')
    self.X = data.inputs.astype(np.float32)
    self.N, self.H, self.W, self.C = self.X.shape
    self.Y = data.targets
    # self.Y_labels = data['labels']
    self.specimen_names = data.specimen_names
    self.excitation_freqs = data.excitation_frequencies
    # self.classes = data['classes']
    self.specimen_idx_to_name = data.specimen_idx_to_name

    self.test_split = config.value('test_split', 0.3)
    self.val_split = config.value('val_split', 0.3)
    self.dim_reduction = config.value('dim_reduction', None)
    self.input_shape = config.value('input_shape', gs.INPUT_SHAPE)
    self.shuffle_specimens = config.value('shuffle_specimens', 'random')
    self.shuffle_pixels = config.value('shuffle_pixels', False)
    self.normalization = config.value('normalization', None)

    self.loss = config.value('loss', 'mae')
    self.class_to_thickness = config.value('class_to_thickness', None)

    self.format_targets()
    self.prepare_dataset()
    logger.debug('Dataset is ready')

  def init_dataset_from_file(self, file_path, test_split=0, val_split=0, input_shape=gs.C, dim_reduction=None,
                             shuffle_specimens=None, shuffle_pixels=False, normalization=None):
    """
    Initializes the dataset directly from the given file path
    :param str file_path:
    :param float test_split:
    :param float val_split:
    :param int|tuple|None input_shape:
    :param str|None|int dim_reduction:
    :param str|list shuffle_specimens:
    :param bool shuffle_pixels:
    :param None|str normalization:
    :return:
    """
    logger.debug('Initializing dataset from file "%s"' % file_path)
    data = self.load_data_from_file(file_path)
    self.X = data.inputs.astype(np.float32)
    self.N, self.H, self.W, self.C = self.X.shape
    self.Y = data.targets
    # self.Y_labels = data['labels']
    self.specimen_names = data.specimen_names
    self.excitation_freqs = data.excitation_frequencies
    # self.classes = data['classes']

    self.test_split = test_split
    self.val_split = val_split
    self.dim_reduction = dim_reduction
    self.input_shape = input_shape
    self.data_path = file_path
    self.shuffle_specimens = shuffle_specimens
    self.shuffle_pixels = shuffle_pixels
    self.normalization = normalization

    self.prepare_dataset()
    logger.debug('Dataset is ready')

  def init_dataset_random(self, N=100, H=512, W=512, C=gs.C, input_shape=gs.C, test_split=0.3, val_split=0.3):
    """
    Initializes a random dataset
    :param int N:
    :param int H:
    :param int W:
    :param int C:
    :param int input_shape:
    :param float|None test_split:
    :param float|None val_split:
    :return:
    """
    logger.debug('Initializing random dataset')
    self.X = np.random.rand(N, H, W, C)
    self.N, self.H, self.W, self.C = self.X.shape
    self.Y = np.random.rand(N, H, W)
    # self.Y_labels = (np.random.rand(N, H, W) * 5).astype(np.uint8)
    self.specimen_names = np.asarray([str(i).zfill(len(str(N))) for i in range(N)])
    self.excitation_freqs = np.asarray([i for i in range(C)])

    self.input_shape = input_shape
    self.test_split = test_split
    self.val_split = val_split

    self.prepare_dataset()
    logger.debug('Dataset is ready')

  def format_targets(self):
    """Formats targets for classification task"""
    # defect versus sound area
    if self.loss == 'sparse_categorical_crossentropy':
      # binary classification
      if self.class_to_thickness is None or isinstance(self.class_to_thickness, str) or\
        (isinstance(self.class_to_thickness, (tuple, list)) and len(self.class_to_thickness) == 2):
        masked = np.ma.masked_equal(self.Y, 0)
        mean = masked.mean()
        # a list is provided to manually set the separation between sound and defect area
        if isinstance(self.class_to_thickness, (tuple, list)) and len(self.class_to_thickness) == 2:
          mean = self.class_to_thickness[0][1]

        self.Y[self.Y > mean] = 0
        self.Y[(self.Y <= mean) & (self.Y > 0)] = 1
        self.Y = self.Y.astype(np.uint8)
      # multi class classification
      elif isinstance(self.class_to_thickness, (tuple, list)):
        t = 0
        new_targets = np.zeros_like(self.Y, np.uint8)
        for class_idx, c_and_t in enumerate(self.class_to_thickness):
          if isinstance(c_and_t, str):
            new_targets[self.Y > t] = class_idx
          else:
            t = c_and_t[1]
            new_targets[(self.Y <= t) & (self.Y > -1)] = class_idx
            self.Y[self.Y <= t] = -1

        self.Y = new_targets



  def prepare_dataset(self):
    """
    Wrapper function to call common preprocessing steps
    :return:
    """
    self.shuffle_dataset()
    self.split_test_set()
    self.reduce_dataset_dimension()
    self.normalize()
    self.split_val_set()
    self.reshape_dataset()
    self.shape_info()
    self.show_statistics()

  def shuffle_dataset(self):
    """
    Shuffles the dataset
    :return:
    """

    if self.shuffle_specimens is not None:
      # random shuffling
      if isinstance(self.shuffle_specimens, str):
        # assert self.shuffle_specimens == 'random'
        self.shuffle_specimens = np.random.permutation(self.N)
      # shuffling order is given
      else:
        assert isinstance(self.shuffle_specimens, (tuple, list)), 'shuffle_specimens has either to a tuple or a list'
      assert len(self.shuffle_specimens) == self.N, '"shuffle_specimens" should have length %d' % self.N
      self.X = self.X[self.shuffle_specimens]
      self.Y = self.Y[self.shuffle_specimens]
      # self.Y_labels = self.Y_labels[self.shuffle_specimens]
      self.specimen_names = self.specimen_names[self.shuffle_specimens]

  def split_test_set(self):
    """
    Splits X, Y and Y_labels into train and test set.
    :return:
    """
    # split X and Y into train and test
    split_idx = self.N - round(self.test_split * self.N)

    self.X_train = self.X[:split_idx]
    self.Y_train = self.Y[:split_idx]
    # self.Y_train_labels = self.Y_labels[:split_idx]
    self.specimen_names_train = self.specimen_names[:split_idx]

    self.X_test = self.X[split_idx:]
    self.Y_test = self.Y[split_idx:]
    # self.Y_test_labels = self.Y_labels[split_idx:]
    self.specimen_names_test = self.specimen_names[split_idx:]

  def reduce_dataset_dimension(self):
    # TODO: Apply PCA? or feature selection k-best (use dim_reduction)
    new_C = self.C
    if self.dim_reduction is not None:
      if isinstance(self.dim_reduction, int):
        pass
      if isinstance(self.dim_reduction, str):
        pass
    # just remove dims
    elif isinstance(self.input_shape, int) and self.input_shape < self.C:
      new_C = self.input_shape
    elif isinstance(self.input_shape, (tuple, list)):
      if self.input_shape[-1] < self.C:
        new_C = self.input_shape[-1]

    assert new_C > 0
    self.X_train = self.X_train[:, :, :, :new_C]
    self.X_test = self.X_test[:, :, :, :new_C]

  def reshape_dataset(self):
    """
    Reshapes train and test set to the according dimensions
    :return:
    """
    # pixel to pixel OR pixel-sequence to pixel
    if isinstance(self.input_shape, int):
      self.X_train = self.X_train.reshape(-1, self.input_shape)
      self.Y_train = self.Y_train.reshape(-1)
      # self.Y_train_labels = self.Y_train_labels.reshape(-1)

      if self.val_split > 0:
        self.X_val = self.X_val.reshape(-1, self.input_shape)
        self.Y_val = self.Y_val.reshape(-1)
        # self.Y_val_labels = self.Y_val_labels.reshape(-1)

      if self.test_split > 0:
        self.X_test = self.X_test.reshape(-1, self.input_shape)
        self.Y_test = self.Y_test.reshape(-1)
      # self.Y_test_labels = self.Y_test_labels.reshape(-1)
    # image to image OR image-sequence to image
    elif isinstance(self.input_shape, (tuple, list)):
      expected_dim = self.input_shape[-1]
      if expected_dim <= self.C:
        self.dim_reduction = expected_dim
        self.reduce_dataset_dimension()
      elif expected_dim > self.C:
        raise Exception('expected dimension (%s) > current dimension (%s)' % (expected_dim, self.C))

      # self.X = self.X.reshape()
      # self.Y = self.

  def split_val_set(self):
    """
    Splits the data into train, validation and test sets and reshapes the inputs and outputs accordingly
    :return:
    """
    #
    # # set data shapes for input and output
    # x_shape, y_shape = self.deduce_data_shape_from_input_shape()
    # # assert len(data_shape) == 2 or len(data_shape) == 4, 'Unsupported data shape: %s' % data_shape
    # self.X_train = self.X_train.reshape(x_shape)
    # self.Y_train = self.Y_train.reshape(y_shape)
    # self.Y_train_labels = self.Y_train_labels.reshape(y_shape)
    #
    # self.X_test = self.X_test.reshape(x_shape)
    # self.Y_test = self.Y_test.reshape(y_shape)
    # self.Y_test_labels = self.Y_test_labels.reshape(y_shape)
    #
    # # shuffle pixels if specified
    # if self.shuffle_pixels:
    #   # pixel-wise training
    #   if len(x_shape) == 2:
    #     num_train_samples = self.X_train.shape[0]
    #     assert num_train_samples == self.Y_train.shape[0]
    #     p = np.random.permutation(num_train_samples)
    #     self.X_train = self.X_train[p]
    #     self.Y_train = self.Y_train[p]
    #     self.Y_train_labels = self.Y_train_labels[p]
    #
    #   elif len(x_shape) == 4:
    #     N, H, W, C = self.X_train.shape
    #     num_pixels = H * W
    #     for n, sample in enumerate(self.X_train):
    #       x_train = sample.reshape(-1, C)
    #       p = np.random.permutation(num_pixels)
    #       x_train = x_train[p]
    #       self.X_train[n] = x_train.reshape(H, W, C)
    #       y_train = self.Y_train[n].reshape(-1)
    #       self.Y_train[n] = y_train[p].reshape(H, W)
    #       y_train_labels = self.Y_train_labels[n].reshape(-1)
    #       self.Y_train_labels[n] = y_train_labels[p].reshape(H, W)

    # create validation set
    if self.val_split > 0:
      num_train_samples = self.X_train.shape[0]
      assert self.val_split < 0.5, 'Validation split should be smaller 0.5'
      assert num_train_samples > 1, 'Not enough training samples to create validation set (%d) < 2' % num_train_samples
      split_idx = max(num_train_samples - round(self.val_split * num_train_samples), 1)
      self.X_val = self.X_train[split_idx:]
      self.Y_val = self.Y_train[split_idx:]
      # self.Y_val_labels = self.Y_train_labels[split_idx:]

      self.X_train = self.X_train[:split_idx]
      self.Y_train = self.Y_train[:split_idx]
      # self.Y_train_labels = self.Y_train_labels[:split_idx]

  @property
  def original_data(self):
    return self.X, self.Y # , self.Y_labels

  @property
  def original_specimens(self):
    return self.specimen_names

  @property
  def train_data(self):
    return self.X_train, self.Y_train # , self.Y_train_labels

  @property
  def train_specimens(self):
    return self.specimen_names_train

  @property
  def val_data(self):
    return self.X_val, self.Y_val # , self.Y_val_labels

  @property
  def test_data(self):
    return self.X_test, self.Y_test # , self.Y_test_labels

  @property
  def test_specimens(self):
    return self.specimen_names_test

  @classmethod
  def load_data_from_file(cls, file_path):
    """
    Loads data from the given file
    :param str file_path:
    :return:
    """
    assert file_path is not None, 'File path is None'
    assert os.path.isfile(file_path), 'Invalid path "%s"' % file_path
    if '.' not in file_path:
      raise Exception('Path "%s" has no file extension' % file_path)

    file_ext = file_path.split('.')[-1]
    file_ext = file_ext.lower()
    data = None
    if file_ext in ['p', 'pickle', 'pkl']:
      f = open(file_path, 'rb')
      data = pickle.load(f)
      f.close()
    elif file_ext in ['hdf', 'h5', 'hdf5', 'he5']:
      pass
    else:
      raise Exception('File extension "%s" not supported' % file_path.split('.')[-1])

    return data

  def deduce_data_shape_from_input_shape(self):
    """
    Deduces data shape form the expected input shape of the input layer
    :return:
    """
    x_shape = None
    y_shape = (-1,)
    if self.input_shape is None:
      # (-1, C), (-1,)
      x_shape = (-1, len(self.excitation_freqs))
    elif isinstance(self.input_shape, int):
      # (-1, C), (-1,)
      x_shape = (-1, self.input_shape)
    elif isinstance(self.input_shape, (list, tuple)):
      assert len(self.input_shape) > 0, 'Length of input_shape has to be greater 0'
      if len(self.input_shape) == 1:
        # (-1, C, 1), (-1,)
        x_shape = (-1,) + self.input_shape
      elif self.input_shape[-1] == 1:
        # (-1, C, 1), (-1, 1) OR (-1, H, W, 1)
        x_shape = (-1,) + self.input_shape
        y_shape = (-1, 1)
      else:
        # (-1, H, W), (-1, H, W) OR (-1, H, W, C), (-1, H, W)
        x_shape = (-1,) + self.input_shape
        y_shape = (-1,) + self.input_shape

    return x_shape, y_shape

  def shape_info(self):
    result = hd_print2('Shape info:')
    result += '{:15s} {}\n'.format('X', self.X.shape if self.X is not None else None)
    result += '{:15s} {}\n'.format('Y', self.Y.shape if self.Y is not None else None)
    # result += '{:15s} {}\n'.format('Y_labels', self.Y_labels.shape if self.Y_labels is not None else None)
    result += '-' * 40 + '\n'
    result += '{:15s} {}\n'.format('X_train:', self.X_train.shape if self.X_train is not None else None)
    result += '{:15s} {}\n'.format('Y_train:', self.Y_train.shape if self.Y_train is not None else None)
    # result += '{:15s} {}\n'.format('Y_train_labels:',
    #                                self.Y_train_labels.shape if self.Y_train_labels is not None else None)
    result += '-' * 40 + '\n'
    result += '{:15s} {}\n'.format('X_val:', self.X_val.shape if self.X_val is not None else None)
    result += '{:15s} {}\n'.format('Y_val:', self.Y_val.shape if self.Y_val is not None else None)
    # result += '{:15s} {}\n'.format('Y_val_labels:', self.Y_val_labels.shape if self.Y_val_labels is not None else None)
    result += '-' * 40 + '\n'
    result += '{:15s} {}\n'.format('X_test:', self.X_test.shape if self.X_test is not None else None)
    result += '{:15s} {}\n'.format('Y_test:', self.Y_test.shape if self.Y_test is not None else None)
    # result += '{:15s} {}\n'.format('Y_test_labels:',
    #                                self.Y_test_labels.shape if self.Y_test_labels is not None else None)
    logger.info(result)

    return result

  def show_statistics(self):
    """
    Shows statistics for the dataset
    :return:
    """
    result = 'Dataset statistics\n'
    cols = ['Min', 'Max', 'Median', 'Mean', 'Variance']
    # data = [self.X_train, self.Y_train, self.Y_train_labels, self.X_val, self.Y_val, self.Y_val_labels, self.X_test,
    #         self.Y_test, self.Y_test_labels]
    # rows = ['X_train', 'Y_train', 'Y_train_labels', 'X_val', 'Y_val', 'Y_val_labels', 'X_test', 'Y_test',
    #         'Y_test_labels']
    data = [self.X_train, self.Y_train, self.X_val, self.Y_val, self.X_test, self.Y_test]
    rows = ['X_train', 'Y_train', 'X_val', 'Y_val', 'X_test', 'Y_test']
    row_format = '{:>20}' * (len(cols) + 1) + '\n'

    result += row_format.format('', *cols)
    for d, r in zip(data, rows):
      stats = compute_statistics(d)
      result += row_format.format(r, *stats)
    logger.info(result)

    return result

  def normalize(self):
    """
    Normalizes the dataset inputs X_train and X_test
    :return: None
    """
    _x = self.X_train.reshape(-1, self.C)
    if self.X_train is not None:
      # scales input data between [0, 1]
      if self.normalization == 'zero-one':
        _min = _x.min(axis=0, keepdims=True)
        _max = _x.max(axis=0, keepdims=True)
        # _min = self.X_train.min()
        # _max = self.X_train.max()
        self.X_train = (self.X_train - _min) / (_max - _min)
        # self.X_train = self.X_train / 255
        if self.X_test is not None:
          # self.X_test = self.X_test / 255
          self.X_test = (self.X_test  - _min) / (_max - _min)
        self.norm_function = lambda x : (x - _min) / (_max - _min)
      # global mean normalization
      elif self.normalization == 'mean':
        mean = np.mean(_x, axis=0, keepdims=True)
        # mean = np.mean(self.X_train)
        self.X_train = self.X_train - mean
        if self.X_test is not None:
          self.X_test = self.X_test - mean
        self.norm_function = lambda x: x - mean
      # global mean-variance normalization
      elif self.normalization == 'mean-variance':
        eps = 1e-7
        mean = np.mean(_x, axis=0, keepdims=True)
        var = np.var(_x, axis=0, keepdims=True)
        # mean = np.mean(self.X_train)
        # var = np.var(self.X_train)
        self.X_train = (self.X_train - mean) / np.sqrt(var + eps)
        if self.X_test is not None:
          self.X_test = (self.X_test - mean) / np.sqrt(var + eps)

        self.norm_function = lambda x: (x - mean) / np.sqrt(var + eps)
      # TODO: phase thickness normalization?
