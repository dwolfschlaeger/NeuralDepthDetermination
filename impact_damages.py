import tensorflow as tf
from tensorflow import keras

import global_settings as gs
# load model
# no normalization
# model_path = 'models/normalization_image_type_inspection_small_fixed_val/models/train_FF_0003_2x16_Do-None_Input-Shape-21_Shuffle-Pixels-True_Img-Type-auto_histogram_Norm-mean-variance.config/best_model.h5'
# model_path = 'models/normalization_image_type_inspection_small_fixed_val/models/train_FF_0032_2x256_Do-None_Input-Shape-21_Shuffle-Pixels-True_Img-Type-auto_histogram_Norm-None.config/best_model.h5'
# model_path = 'models/normalization_image_type_inspection_small_fixed_val/models/train_LSTM_0020_2x64_Do-None_Input-Shape-21_Shuffle-Pixels-True_Img-Type-full_histogram_Norm-None.config/best_model.h5'

# models wit 14 excitation frequencies
model_paths = [
  'models/impact_damages_0_1_norm/models/train_FF_000_2x128.config/best_model.h5',
  'models/impact_damages_0_1_norm/models/train_FF_001_8x64.config/best_model.h5',
  'models/impact_damages_0_1_norm/models/train_FF_002_8x64_mse.config/best_model.h5',
  'models/impact_damages_0_1_norm/models/train_FF_003_2x128_mse.config/best_model.h5',
  'models/impact_damages_0_1_norm/models/train_FF_004_1x2048.config/best_model.h5',
  'models/impact_damages_0_1_norm/models/train_FF_005_10x16.config/best_model.h5',
  'models/impact_damages_0_1_norm/models/train_FF_006_10x16_mae_tanh.config/best_model.h5'
]

# load images
import glob
import numpy as np
import cv2
import matplotlib.pyplot as plt
import re
import os

# H = W = 512
exc_freqs = [0.5, 0.4, 0.299, 0.2, 0.15, 0.1, 0.09, 0.08, 0.07, 0.06, 0.05, 0.04, 0.03, 0.02]
H = W = 490
C = len(exc_freqs)
img_root = './data/impact_damages'
output_dir = os.path.join(img_root, 'output_0_1_norm_qualitative_colormap')

for model_path in model_paths:
  model = tf.keras.models.load_model(
    model_path,
    custom_objects=None,
    compile=True
  )
  for specimen_dir in glob.glob(img_root + '/*/*'):
    # imgs_per_specimen = np.ndarray((H, W, 21), dtype=float)
    imgs_per_specimen = np.ndarray((H * W, C), dtype=float)
    img_paths = glob.glob(specimen_dir + '/*.bmp')
    # assert len(img_paths) == C
    title = specimen_dir.split('/')[-1]
    counter = 0
    print(title)
    for idx, img_path in enumerate(img_paths):
      match = re.search('[0-9]*,[0-9]*Hz', img_path)
      if match:
        exc_freq = float(match[0][0:-2].replace(',', '.'))
        if exc_freq in exc_freqs:
          img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
          img = img.reshape(-1)
          img = img / 255
          # img = cv2.resize(img, (H, W))
          imgs_per_specimen[:, counter] = img
          counter += 1
          print('counter', counter)


    if counter == C:
      y = model.predict(imgs_per_specimen)
      y = y.reshape((H, W))
      plt.imshow(y)
      cb = plt.colorbar()
      cb.ax.set_ylabel('Thickness/mm')
      plt.ylabel('Pixel')
      plt.xlabel('Pixel')
      plt.title(title)
      _dir = os.path.join(output_dir, model.name)
      os.makedirs(_dir, exist_ok=True)
      output = os.path.join(_dir, title)
      print('Saving under: %s' % output)
      # plt.savefig(output)
      # plt.close()

      # plt.imsave(title)

      plt.show()
