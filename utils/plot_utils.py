import os
import copy
import numbers
import logging
import cv2

import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import seaborn as sns
import numpy as np
from matplotlib import lines, markers, cm, colors
from cycler import cycler
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

from .generic_utils import hd_print, min_max_norm
from . import img_utils
import global_settings as gs

logger = logging.getLogger('NDD')
# Create cycler object. Use any styling from above you please
monochrome = (cycler('color', ['k']) * cycler('linestyle', ['-', '--']) * cycler('marker', ['^']))

# set readable font size
# plt.style.use('default')
matplotlib.rcParams.update({
  # 'pgf.texsystem': 'pdflatex',
  'font.family': 'serif',
  'font.size': 12,
  'axes.labelsize': 12,
  # 'text.usetex': True,
  'legend.fontsize': 10,
  'xtick.labelsize': 10,
  'ytick.labelsize': 10,
  # 'pgf.rcfonts': False,
  # 'pdf.fonttype': 42
})


def plot_histogram(img, bins=256, x_ticks=None, x_label=None, title='', output_dir=None, prefix=None):
  # assert isinstance(img, np.ndarray)
  # if np.issubdtype(img.dtype, numbers.Integral):
  #   print('rounding')
  #   img = np.around(img, 1)
  # labels, counts = np.unique(np.ravel(img), return_counts=True)
  # print('labels', labels)
  # plt.bar(labels, counts, align='edge')
  # plt.gca().set_xticks(labels)

  plt.hist(img.ravel(), bins, align='left')
  # plt.title(title)
  if x_ticks is not None:
    plt.xticks(x_ticks)
  plt.ylabel('counts')
  if x_label is not None:
    plt.xlabel(x_label)

  filename = title.lower().replace(' ', '_')
  save_or_show_plot(output_dir, None, filename)


def plot_histograms(imgs, title='all imgs', bins=256, x_label=None):
  flat_imgs = np.array([])
  for key, img in imgs.items():
    flat_imgs = np.append(flat_imgs, img.ravel())
  plot_histogram(flat_imgs, title, bins, x_label)


def plot_ground_truth_vs_prediction(y, y_pred, gs, cmap='plasma', output_dir=None, prefix=None):
  """
  Plots the ground truth and the prediction
  :param np.ndarray y: ground truth with shape (H, W)
  :param np.ndarray y_pred: prediction with shape (H, W)
  :param gs: global settings
  :param str cmap:
  :param str|None output_dir:
  :param str|int prefix:
  :return:
  """

  vmin = min(y.min(), y_pred.min())
  vmax = max(y.max(), y_pred.max())

  y = y.reshape(gs.H, gs.W)
  y_pred = y_pred.reshape(gs.H, gs.W)
  # xticks = [0, gs.W - 1]
  # yticks = [0, gs.H - 1]
  # extent = (0, gs.W, 0, gs.H)

  xticks = [0.0, gs.W * gs.PIXEL2MM]
  yticks = [0.0, gs.H * gs.PIXEL2MM]
  extent = (0.0, gs.W * gs.PIXEL2MM, 0.0, gs.H * gs.PIXEL2MM)
  print('Extent', extent)
  _im = None

  fig, axes = plt.subplots(nrows=1, ncols=2, sharex='all', sharey='all')
  for i, ax in enumerate(axes):
    if i == 0:
      _y = y
      title = 'ground truth'
    else:
      _y = y_pred
      title = 'prediction'
    ax.set_title(title)
    _im = ax.imshow(_y, vmin=vmin, vmax=vmax, extent=extent)
    ax.set_xticks(xticks)
    ax.set_yticks(yticks)
    ax.set_xlabel('width/mm')
    if i == 0: # y is shared
      ax.set_ylabel('height/mm')

  if _im is not None:
    # fig.subplots_adjust(bottom=0.1, right=0.8, top=0.9, hspace=0.5)
    fig.subplots_adjust(right=0.8, bottom=0.008)
    cax = fig.add_axes([0.84, 0.2, 0.02, 0.45])
    cb = fig.colorbar(_im, cax=cax)
    cb.set_label('thickness/mm')

  save_or_show_plot(output_dir, prefix, 'ground_truth_vs_prediction.svg')


def plot_ground_truth_vs_prediction_all(y, y_pred, gs, output_dir):
  # matplotlib.rcParams.update({
  #   'font.family': 'serif',
  #   'font.size': 12,
  #   'axes.labelsize': 12,
  #   'legend.fontsize': 10,
  #   'xtick.labelsize': 10,
  #   'ytick.labelsize': 10,
  # })

  vmin = min(y.min(), y_pred.min())
  vmax = max(y.max(), y_pred.max())

  xticks = [0.0, gs.W * gs.PIXEL2MM]
  yticks = [0.0, gs.H * gs.PIXEL2MM]
  extent = (0.0, gs.W * gs.PIXEL2MM, 0.0, gs.H * gs.PIXEL2MM)

  num_samples = y.shape[0]
  fig, axis = plt.subplots(nrows=num_samples, ncols=2, sharex='all', sharey='all', squeeze=True,
                           gridspec_kw={'wspace': 0.0, 'hspace': .15})
  axis[0][0].set_title('ground truth')
  axis[0][1].set_title('prediction')

  axis[0][0].set_xticks(xticks)
  axis[0][0].set_yticks(yticks)
  axis[0][1].set_xticks(xticks)
  axis[0][1].set_yticks(yticks)

  plt.setp(axis[-1, :], xlabel='width/mm')
  plt.setp(axis[:, 0], ylabel='height/mm')
  _im = None
  for idx, ax in enumerate(axis):
    ax[0].imshow(y[idx], vmin=vmin, vmax=vmax, extent=extent)
    _im = ax[1].imshow(y_pred[idx], vmin=vmin, vmax=vmax, extent=extent)

  fig.subplots_adjust(right=0.9, bottom=0.008)
  cax = fig.add_axes([0.84, 0.03, 0.02, 0.83])
  cb = fig.colorbar(_im, cax=cax)
  cb.set_label('thickness/mm')

  filename = 'all_ground_truths_vs_predictions.svg'
  plt.savefig(os.path.join(output_dir, filename), bbox_inches='tight', dpi=300)
  plt.close()


def plot_ground_truth_vs_prediction_flattened(y, y_pred, slicing_step=1000, output_dir=None, prefix=None):
  y = y.reshape(-1)
  sort_indices = np.argsort(y)
  y = y[sort_indices]
  y_pred = y_pred.reshape(-1)
  y_pred = y_pred[sort_indices]

  slicing_step_str = ''
  if slicing_step and slicing_step > 0:
    y = y[0::slicing_step]
    y_pred = y_pred[0::slicing_step]
  y_ticks = np.arange(int(min(y.min(), y_pred.min())), int(max(y.max(), y_pred.max())), 1)

  plt.plot(y, 'g', label='real thickness')
  plt.plot(y_pred, 'r--', alpha=0.6, label='predicted thickness')
  plt.xlabel('sample')
  plt.ylabel('thickness/mm')
  # plt.yticks(y_ticks)
  # plt.title('ground truth versus prediction%s' % slicing_step_str)
  plt.legend()
  save_or_show_plot(output_dir, prefix, 'ground_truth_vs_prediction_flattened')


def plot_single_image(img, title='Title', cmap='plasma', vmin=None, vmax=None, output_dir=None):
  """
  Plots a single image
  :param np.ndarray img: image with the shape of (H, W)
  :param str title:
  :param str cmap:
  :param int|float vmin:
  :param int|float vmax:
  :param str output_dir: optional output path where the plot can be saved
  :return:
  """
  plt.imshow(img, cmap=cmap, vmin=vmin, vmax=vmax)
  plt.title(title)
  plt.xlabel('pixel')
  plt.ylabel('pixel')
  cb = plt.colorbar()
  cb.set_label('thickness/mm')

  if output_dir and os.path.isdir(output_dir):
    plt.savefig('%s.png' % title.replace(' ', '_'))
    plt.close()
  else:
    plt.show()


def plot_grayvalue_against_frequency(phase_imgs, labels, points, exc_freqs, output_dir='.'):
  """

  :param numpy.ndarray phase_imgs: (H, W, C)
  :param numpy.ndarray labels: (H, W)
  :param list points:n
  :param list exc_freqs:
  :param str output_dir:
  """

  for w, h in points:
    label = '%.2f mm' % labels[h, w]
    y = phase_imgs[h, w, :]
    assert len(y) == len(exc_freqs)
    plt.plot(exc_freqs, y, label=label)
    plt.plot(exc_freqs, y, 'r', marker='x', linestyle='None')

  plt.ylabel('gray value')
  plt.xscale('log')
  plt.xlabel('frequency/Hz')
  title = 'Gray value course depending on the frequency'
  plt.title(title)
  plt.legend()
  plt.grid(True)

  if output_dir and isinstance(output_dir, str):
    if not os.path.isdir(output_dir):
      os.mkdir(output_dir)
    title = title.replace(' ', '_')
    output_path = os.path.abspath(os.path.join(output_dir, title))
    plt.savefig(output_path)

  plt.show()


def plot_metrics_from_history(hist, model_dir=None):
  output_dir = None
  if model_dir and os.path.isdir(model_dir):
    output_dir = os.path.join(model_dir, 'metrics')
    if not os.path.isdir(output_dir):
      os.mkdir(output_dir)

  history = copy.copy(hist.history)
  hist_keys = history.keys()
  val_metrics = [m for m in hist_keys if 'val_' in m]

  for val_m in val_metrics:
    m = val_m[4:]
    if m in hist_keys:
      fig, ax = plt.subplots(1, 1)
      # ax.set_prop_cycle(monochrome)
      ax.grid()

      y = history.pop(m)
      ax.plot(y, label=m)

      y_val = history.pop(val_m)
      ax.plot(y_val, label=val_m)

      ax.legend()
      ax.set_xlabel('epoch')
      y_label = 'accuracy' if 'accuracy' in val_m else 'loss'
      ax.set_ylabel(y_label)
      # x_ticks = list(set(np.linspace(0, len(y), 10, dtype=np.uint8)))
      # ax.set_xticks(x_ticks)
      # ax.set_xticklabels(x_ticks)
      # ax.set_xlim([x_ticks[0], x_ticks[-1]])

      if output_dir and os.path.isdir(output_dir):
        plt.savefig(os.path.join(output_dir, '%s.svg' % m.capitalize()), bbox_inches='tight')
        plt.close()
      else:
        plt.show()


def plot_errors_per_bin(y, y_pred, use_min_dist_to_bin=True, output_dir=None, prefix=None,
                        metrics=('mae', 'mse', 'rmse', 'r2'),
                        bins=(0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0)):
  """
  :param y:
  :param y_pred:
  :param output_dir:
  :param use_min_dist_to_bin:
  :param prefix:
  :param metrics:
  :param bins:
  :return:
  """

  # reshape and sort both arrays
  y = y.reshape(-1)
  y_pred = y_pred.reshape(-1)
  sort_indices = np.argsort(y)
  y = y[sort_indices]
  y_pred = y_pred[sort_indices]

  # compute differences between prediction and ground truth for each bin
  num_bins = len(bins)
  bin_diffs = [[] for i in range(num_bins)]
  if use_min_dist_to_bin:
    for i, _y in enumerate(y):
      bin_idx = np.argmin(np.abs(bins - _y))
      diff = (_y - y_pred[i])
      bin_diffs[bin_idx].append(diff)
  else:
    for i, thresh in enumerate(bins):
      bin_indices = np.argwhere(y <= thresh)
      bin_diffs[i] = y[bin_indices] - y_pred[bin_indices]
      y = np.delete(y, bin_indices)
      y_pred = np.delete(y_pred, bin_indices)

    remaining_diff = (y - y_pred).reshape(-1, 1)
    bin_diffs[i] = np.concatenate((bin_diffs[i], remaining_diff), axis=0)

  # create plot
  fig = plt.figure()
  ax = fig.add_subplot(111)

  all_diffs = np.asarray([x for sublist in bin_diffs for x in sublist])
  global_error = -1
  for i, m in enumerate(metrics):
    if m == 'mae':
      bin_errors = [np.mean(np.abs(diff)) for diff in bin_diffs]
      global_error = np.mean(np.abs(all_diffs))
    elif m == 'mse':
      bin_errors = [np.mean(np.square(diff)) for diff in bin_diffs]
      global_error = np.mean(np.square(all_diffs))
    elif m == 'rmse':
      bin_errors = [np.sqrt(np.mean(np.abs(diff))) for diff in bin_diffs]
      global_error = np.sqrt(np.mean(np.abs(all_diffs)))
    elif m == 'mape':
      bin_errors = [100 * np.mean(np.abs(diff)) for diff in bin_diffs]
      global_error = 100 * np.mean(np.abs(all_diffs))
    elif m == 'r2':
      # bin errors not correct here
      bin_errors = [1 - ((np.square(diff)).sum() / (np.square(diff - np.mean(diff))).sum()) for diff in bin_diffs]
      global_error = 1 - ((np.square(all_diffs)).sum() / (np.square(y - np.mean(y))).sum())
    else:
      continue

    # TODO: write them to file?
    print('Total error %4s: %f' % (m, global_error))

    plt.plot(bins, bin_errors, '-x', label='%s [%.3f]' % (m, global_error))
    for k, l in zip(bins, bin_errors):
      l = round(l, 3)
      ax.annotate(str(l), xy=(k, l))
  # plt.title('Errors for each bin')
  plt.xticks(bins)
  plt.legend()
  plt.grid()

  save_or_show_plot(output_dir, prefix, 'errors_per_bin')


def plot_ground_truth_and_prediction_histograms(y, y_pred, output_dir=None, prefix=None, bins=(0, 1, 2, 3, 4)):
  # bins=[-1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0]
  y = y.reshape(-1)
  y_pred = y_pred.reshape(-1)
  _min = min(y.min(), y_pred.min(), -1)
  _max = max(y.max(), y_pred.max(), bins[-1])
  _bins = [_min]
  for _bin in bins:
    _bins.append(_bin)
  _bins.append(_max)
  _bins = bins
  # bins = np.linspace(min(y.min(), y_pred.min()), max(y.max(), y_pred.max()), num_bins)
  plt.xticks(_bins)
  hist_y = plt.hist(y, _bins, fc=(0, 1, 0, 1), ls='dashed', align='mid', label='Ground truth')
  hist_y_pred = plt.hist(y_pred, _bins, fc=(1, 0, 0, 0.5), ls='dotted', align='mid', label='Prediction')
  plt.xlabel('thickness/mm')
  plt.ylabel('count')
  plt.title('Histograms of ground truth and prediction')
  plt.legend()

  save_or_show_plot(output_dir, prefix, 'ground_truth_and_prediction_histograms')


def plot_input_target_and_output(x, y, y_label=None, y_pred=None, title='', output_dir=None, prefix=None):
  """
  Plots the input its target and optionally its target label and the prediction
  :param int|np.array_like x:
  :param float|np.array_like y:
  :param int|np.array_like y_label:
  :param float|np.array_like y_pred:
  :param str title:
  :param None|str output_dir:
  :param int|str prefix:
  :return:
  """
  ncols = 2
  show_label = False
  show_output = False
  x_x_ticks = [0, 1]
  x_y_ticks = [0, 1]
  y_x_ticks = [0, 1]
  y_y_ticks = [0, 1]
  x_extent = (0, 1, 0, 1)
  y_extent = (0, 1, 0, 1)
  _vmin = None
  _vmax = None
  y_tmp = None

  # setup number of subplots
  if y_label is not None and y_pred is not None:
    ncols = 4
    show_label = True
    show_output = True
  elif y_label is not None or y_pred is not None:
    ncols = 3
    if y_label is not None:
      show_label = True
    else:
      show_output = True

  fig, axes = plt.subplots(nrows=1, ncols=ncols, figsize=(10, 10))
  # fig.suptitle('Specimen %s' % title)
  axes[0].set_title('input')
  axes[1].set_title('target')
  if show_label and show_output:
    axes[2].set_title('target labels')
    axes[3].set_title('outputs')
  elif show_label or show_output:
    sub_title = 'target labels' if show_label else 'outputs'
    axes[2].set_title(sub_title)

  # target is pixel
  if isinstance(y, numbers.Number):
    # pixel to pixel
    if isinstance(x, numbers.Number):
      x = np.asarray([[x]])
      y = np.asarray([[y]])
      _vmin = min(x, y)
      _vmax = max(x, y)
    # pixel sequence to pixel
    elif isinstance(x, np.ndarray) and len(x.shape) == 1:
      x = x.reshape((1, 1, x.shape[0]))
      y = np.asarray([[y]])
      _vmin = min(x.min(), y.min())
      _vmax = max(x.max(), y.max())
      x = img_utils.create_stacked_image(x)

    # handle y_label and y_pred
    if ncols == 4:
      assert isinstance(y_label, numbers.Number) and isinstance(y_pred, numbers.Number)
      y_label = np.asarray([[y_label]])
      y_pred = np.asarray([[y_pred]])
      _vmin = min(_vmin, y_label, y_pred)
      _vmax = max(_vmax, y_label, y_pred)
    elif ncols == 3:
      y_tmp = y_label if show_label else y_pred
      y_tmp = np.asarray([[y_tmp]])
      _vmin = min(_vmin, y_tmp)
      _vmax = max(_vmax, y_tmp)

    height, width = x.shape
    x_x_ticks = np.arange(0, width)
    x_y_ticks = np.arange(0, height)
    x_extent = (0, width, 0, height)


  # target is an image
  elif isinstance(y, np.ndarray) and len(y.shape) == 2:
    height, width = y.shape
    y_y_ticks = [0, height * gs.PIXEL2MM]
    y_x_ticks = [0, width * gs.PIXEL2MM]
    y_extent = (0, width * gs.PIXEL2MM, 0, height * gs.PIXEL2MM)
    # y_extent = (0, width, 0, height)
    # y_y_ticks = [0, height - 1]
    # y_x_ticks = [0, width - 1]
    # image to image
    if isinstance(x, np.ndarray) and len(x.shape) == 2:
      height, width = x.shape
      x_extent = (0, width, 0, height)
    # image sequence to image
    elif isinstance(x, np.ndarray) and len(x.shape) == 3:
      x = img_utils.create_stacked_image(x, offset_x=50, offset_y=50)
    if ncols == 3:
      y_tmp = y_label if show_label else y_pred

  for i, ax in enumerate(axes):
    if i == 0:
      ax.imshow(x, cmap='gray', vmin=_vmin, vmax=_vmax, extent=x_extent)
      # ax.set_xticks(x_x_ticks)
      # ax.set_xticklabels(x_x_ticks)
      # ax.set_yticks(x_y_ticks)
      # ax.set_yticklabels(x_y_ticks)
      ax.set_axis_off()
    else:
      if i == 1:
        _y = y
      elif i == 2 and ncols == 3:
        _y = y_tmp
      else:
        _y = y_label
      if i == 3:
        _y = y_pred
      im = ax.imshow(_y, vmin=_vmin, vmax=_vmax, extent=y_extent)
      if i == 1:
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.08)
        cb = fig.colorbar(im, ax=ax, cax=cax)
        cb.set_label('thickness/mm')
      ax.set_xticks(y_x_ticks)
      ax.set_yticks(y_y_ticks)
      ax.set_xlabel('width/mm')
      ax.set_ylabel('height/mm')

  # plt.tight_layout()
  filename = '%s' % title
  save_or_show_plot(output_dir, prefix=prefix, filename=filename)


def plot_boreholes_depth_deviation(y, y_pred, gs, output_dir=None, prefix=None, neighbouring_pixel_radius=3):
  """
  Computes the the depth deviations between y and y_pred in the borehole areas
  :param y: ground truth
  :param y_pred: prediction
  :param gs: global_settings
  :param output_dir:
  :param prefix:
  :param int neighbouring_pixel_radius: the radius in which the deviation will be computed e.g.
  neighbouring_pixel_radius=3 results in 29 pixels
  :return:
  """
  y = y.reshape(gs.H, gs.W)
  y_pred = y_pred.reshape(gs.H, gs.W)

  y_img, y_circles = img_utils.segment_circles(y, y_pred)
  plt.imshow(y_img)
  save_or_show_plot(output_dir, prefix, 'boreholes_depth_deviation')

  if len(y_circles[0]) != 30:
    return

  true_thickness = []
  estimated_thickness = []

  deviations = []
  for x1, y1, radius in y_circles[0]:
    circle_mask = img_utils.create_circular_mask(gs.H, gs.W, (x1, y1), neighbouring_pixel_radius)
    y_borehole = (circle_mask * y).flatten()
    y_pred_borehole = (circle_mask * y_pred).flatten()
    # TODO: deviations sometimes NaN
    y_depth = np.median(y_borehole[np.nonzero(y_borehole)])
    y_pred_depth = np.median(y_pred_borehole[np.nonzero(y_pred_borehole)])

    true_thickness.append(y_depth)
    estimated_thickness.append(y_pred_depth)

    # deviation = abs(y_depth - y_pred_depth) * 100
    deviation = (y_depth - y_pred_depth) * 100
    deviations.append(deviation)

    # deviation = abs(y_borehole - y_pred_borehole).flatten()
    # deviation = deviation[np.nonzero(deviation)]
    # deviation = sum(deviation) / len(deviation)
    # deviations.append(deviation * 100)

  # to validate whether predictions are within the combined measurement uncertainty from Ekanayake's PhD. thesis
  # print('y')
  # print(true_thickness)
  # print('y_pred')
  # print(estimated_thickness)
  # print('Whole with 16mm diameter and thickness 1mm')
  # print(true_thickness[23])
  # print(estimated_thickness[23])
  total_deviation = sum(deviations) / len(deviations)
  logger.info('Percentage depth deviations for each hole:')
  dev_per_hole = ''
  for idx, dev in enumerate(deviations):
    dev_per_hole += '{:.2f}\t'.format(dev)
    if (idx + 1) % 5 == 0:
      logger.info(dev_per_hole)
      dev_per_hole = ''

  logger.info('Average depth deviation for specimen %s: %f' % (prefix, total_deviation))

  # heatmap
  deviations = np.asarray(deviations).reshape(6, 5)
  # check for nan
  nans = np.isnan(deviations)
  deviations[nans] = -1

  xlabels = [4, 8, 12, 16, 20]
  ylabels = [3.0, 2.5, 2.0, 1.5, 1.0, 0.5]
  vmin = min(deviations.min(), -deviations.max())
  vmax = max(deviations.max(), abs(deviations.min()))
  # fig, ax = plt.subplots(figsize=(10, 10))

  fig, ax = plt.subplots()
  cbar_kws = {'label': 'depth deviation/%', }
  annot_kws = {'size': 11}
  sns.heatmap(deviations, annot=True, xticklabels=xlabels, yticklabels=ylabels, fmt='.2f', cbar_kws=cbar_kws, ax=ax,
              linewidths=.1, vmin=vmin, vmax=vmax, center=0, cmap='RdYlBu_r', annot_kws=annot_kws)
  # plt.title(
  #   'Percentage borehole depth deviations for specimen %s (radius=%d Pixel)' % (prefix, neighbouring_pixel_radius))
  # plt.ylabel('thickness/mm', fontsize=12)
  # plt.xlabel('diameter/mm', fontsize=12)
  plt.ylabel('thickness/mm')
  plt.xlabel('diameter/mm')
  save_or_show_plot(output_dir, prefix, 'boreholes_depth_deviation_heatmap.svg')

  return deviations


def plot_average_boreholes_depth_deviations(average_depth_deviations, percentage=True, suffix='mape', output_dir=None):
  # Source https://stackoverflow.com/questions/38246559/how-to-create-a-heat-map-in-python-that-ranges-from-green-to-red
  suffix_to_label = {
    'mape': 'mean absolute thickness deviation/%',
    'mpe': 'mean thickness deviation/%',
    'mae' : 'mean absolute thickness deviation/mm',
    'me': 'mean thickness deviation/mm',
    'rmse': 'root mean squared thickness deviation/mm'
  }
  cbar_kws = {'label': suffix_to_label[suffix]}

  # This dictionary defines the colormap
  cdict = {'red': ((0.0, 0.0, 0.0),  # no red at 0
                   (0.5, 1.0, 1.0),  # all channels set to 1.0 at 0.5 to create white
                   (1.0, 0.8, 0.8)),  # set to 0.8 so its not too bright at 1
           'green': ((0.0, 0.8, 0.8),  # set to 0.8 so its not too bright at 0
                     (0.5, 1.0, 1.0),  # all channels set to 1.0 at 0.5 to create white
                     (1.0, 0.0, 0.0)),  # no green at 1
           'blue': ((0.0, 0.0, 0.0),  # no blue at 0
                    (0.5, 1.0, 1.0),  # all channels set to 1.0 at 0.5 to create white
                    (1.0, 0.0, 0.0))  # no blue at 1
           }
  # Create the colormap using the dictionary
  GnRd = colors.LinearSegmentedColormap('GnRd', cdict)
  cmap = LinearSegmentedColormap(
    'GreenRed',
    {
      'red': ((0.0, 0.0, 0.0),
              (0.5, 1.0, 1.0),
              (1.0, 1.0, 1.0)),
      'green': ((0.0, 1.0, 1.0),
                (0.5, 1.0, 1.0),
                (1.0, 0.0, 0.0)),
      'blue': ((0.0, 0.0, 0.0),
               (0.5, 1.0, 1.0),
               (1.0, 0.0, 0.0))
    }, )
  xlabels = [4, 8, 12, 16, 20]
  ylabels = [3.0, 2.5, 2.0, 1.5, 1.0, 0.5]
  vmin = average_depth_deviations.min()
  vmax = average_depth_deviations.max()

  if suffix in ['me', 'mpe']:
    cmap = 'RdYlBu_r'
    vmin = min(average_depth_deviations.min(), -average_depth_deviations.max())
    vmax = max(average_depth_deviations.max(), abs(average_depth_deviations.min()))

  fig, ax = plt.subplots()

  annot_kws = {'size': 11}
  # sns.heatmap(average_depth_deviations, annot=True, xticklabels=xlabels, yticklabels=ylabels, fmt='.2f',
  #             cbar_kws=cbar_kws, ax=ax, linewidths=.1, vmin=vmin, vmax=vmax, center=0, cmap=cmapGR, annot_kws=annot_kws)
  sns.heatmap(average_depth_deviations, annot=True, xticklabels=xlabels, yticklabels=ylabels, fmt='.2f',
              cbar_kws=cbar_kws, ax=ax, linewidths=.1, vmin=vmin, vmax=vmax, cmap=cmap, annot_kws=annot_kws)
  plt.ylabel('thickness/mm')
  plt.xlabel('diameter/mm')
  save_or_show_plot(output_dir, filename='average_boreholes_depth_deviations_%s.svg' % suffix)


def plot_boreholes_area_deviation(y, y_pred, gs, cmap='plasma', output_dir=None, prefix=None, use_cheating=False):
  """
  Computes the area deviations between y and y_pred in the borehole areas, by first finding the circles in y and
  afterwards for each found circle the closest neighbouring circle with the smallest Jaccard distance in y_pred is found.
  :param y:
  :param y_pred:
  :param gs: globa_settings
  :param str cmap:
  :param str|None output_dir:
  :param str|int prefix:
  :param bool use_cheating:
  :return:
  """
  y = y.reshape(gs.H, gs.W)
  y_pred = y_pred.reshape(gs.H, gs.W)

  y_img, y_circles = img_utils.segment_circles(y, y_pred)
  if use_cheating:
    y_pred_img, y_pred_circles = img_utils.segment_circles(y_pred, y_pred, circle_color=(255, 0, 0), min_dist=1,
                                                           param1=1, param2=1, min_radius=1)
  else:
    y_pred_img, y_pred_circles = img_utils.segment_circles(y_pred, y_pred, circle_color=(255, 0, 0), min_dist=1,
                                                           param1=100, param2=12)

  if len(y_circles[0]) != 30:
    return

  deviations = []
  closest_circles = []
  for x1, y1, r1 in y_circles[0]:
    circle, deviation = img_utils.find_closest_circle_and_overlapping_area(x1, y1, r1, y_pred_circles)
    deviation = 100 * deviation
    deviations.append(deviation)
    closest_circles.append(circle)

  if y_pred.dtype != np.uint8:
    y_pred = min_max_norm(y_pred, 0, 255, dtype=np.uint8)

  y_pred = cv2.cvtColor(y_pred, cv2.COLOR_GRAY2BGR)
  for idx, [x1, y1, r1] in enumerate(y_circles[0]):
    cv2.circle(y_pred, (x1, y1), r1, (0, 255, 0), 2)

    if closest_circles[idx] is not None:
      x2, y2, r2 = closest_circles[idx]
      cv2.circle(y_pred, (x2, y2), r2, (255, 0, 0), 2)

  total_deviation = sum(deviations) / len(deviations)
  logger.info('Percentage area deviations for each hole for specimen %s:' % prefix)
  dev_per_hole = ''
  for idx, dev in enumerate(deviations):
    dev_per_hole += '{:.2f}\t'.format(dev)
    if (idx + 1) % 5 == 0:
      logger.info(dev_per_hole)
      dev_per_hole = ''

  logger.info('Mean percentage area deviation for specimen %s: %f' % (prefix, total_deviation))

  # save img
  plt.imshow(y_pred)
  save_or_show_plot(output_dir, prefix, 'boreholes_area_deviation')


def plot_boreholes_area_deviation_by_convex_hull(y, y_pred, gs, cmap='plasma', output_dir=None, prefix=None):
  y = y.reshape(gs.H, gs.W)
  y_pred = y_pred.reshape(gs.H, gs.W)

  y = min_max_norm(y, 0, 255, np.uint8)
  y_pred = min_max_norm(y_pred, 0, 255, np.uint8)
  cv2.imwrite(os.path.join(output_dir, 'ground_truth.png'), y)

  cv2.imwrite(os.path.join(output_dir, 'prediction.png'), y_pred)

  img_utils.find_convex_hulls(y)
  img_utils.find_convex_hulls(y_pred)


def plot_confusion_matrix(confusion_matrix, class_to_thickness, output_dir):
  """
  Creates a normalized confusion matrix plot
  :param tf.Tensor confusion_matrix:
  :param list class_to_thickness:
  :param str output_dir:
  :return:
  """
  _cm = confusion_matrix.numpy()

  # extract class labels only
  if class_to_thickness is not None:
    labels = [c if isinstance(c, str) else c[0] for c in class_to_thickness]
  else:
    labels = ['non-defect', 'defect']

  # normalize confusion matrix
  _cm = np.array([value / row.sum() for row in _cm for value in row]).reshape(_cm.shape)
  # create heatmap
  heatmap = sns.heatmap(_cm, annot=True, fmt='.2f', cmap=plt.cm.Blues)
  heatmap.yaxis.set_ticklabels(labels, rotation=0)
  heatmap.xaxis.set_ticklabels(labels, rotation=0)
  plt.ylabel('true label')
  plt.xlabel('predicted label')

  save_or_show_plot(output_dir, filename='confusion_matrix.svg')


def plot_classification_result(y, y_pred, gs, class_to_thickness, output_dir=None, prefix=None):
  import matplotlib.cm as cm
  cmap = cm.brg
  # # cmap = cm.gist_heat
  # # cmap = cm.Set3
  xticks = [0.0, gs.W * gs.PIXEL2MM]
  yticks = [0.0, gs.H * gs.PIXEL2MM]
  extent = (0.0, gs.W * gs.PIXEL2MM, 0.0, gs.H * gs.PIXEL2MM)

  # extract class labels only
  if class_to_thickness is not None:
    labels = [c if isinstance(c, str) else c[0] for c in class_to_thickness]
  else:
    labels = ['non-defect', 'defect']


  plt.figure(1)
  ax = plt.subplot(121)
  ax.set_title('ground truth')
  plt.ylabel('height/mm')
  plt.xlabel('width/mm')
  plt.imshow(y, cmap=cmap, vmin=0, vmax=2, extent=extent)

  plt.xticks(xticks)
  plt.yticks(yticks)
  ax = plt.subplot(122)
  ax.set_title('prediction')
  # plt.ylabel('height/mm')
  plt.xlabel('width/mm')
  plt.imshow(y_pred, cmap=cmap, vmin=0, vmax=2, extent=extent)

  plt.xticks(xticks)
  plt.yticks(yticks)
  save_or_show_plot(output_dir, prefix, 'classification.svg')


def plot_all_classification_results(y, y_pred, class_to_thickness, output_dir):
  import matplotlib.patches as mpatches
  import matplotlib.cm as cm

  cmap = cm.brg

  # extract class labels only
  if class_to_thickness is not None:
    labels = [c if isinstance(c, str) else c[0] for c in class_to_thickness]
  else:
    labels = ['non-defect', 'defect']

  # hard coded to distinguish binary and three class classification in the same plot
  vmin = 0
  vmax = 2
  # pixel to mm
  xticks = [0.0, gs.W * gs.PIXEL2MM]
  yticks = [0.0, gs.H * gs.PIXEL2MM]
  extent = (0.0, gs.W * gs.PIXEL2MM, 0.0, gs.H * gs.PIXEL2MM)
  fig_kwargs = {
    'figsize': (10, 10)
  }
  num_samples = y.shape[0]
  fig, axis = plt.subplots(nrows=num_samples, ncols=2, sharex='all', sharey='all',
                           gridspec_kw={'wspace': 0.0, 'hspace': 0.1}, **fig_kwargs)
  axis[0][0].set_title('ground truth')
  axis[0][1].set_title('prediction')

  axis[0][0].set_xticks(xticks)
  axis[0][0].set_yticks(yticks)
  axis[0][1].set_xticks(xticks)
  axis[0][1].set_yticks(yticks)

  plt.setp(axis[-1, :], xlabel='width/mm')
  plt.setp(axis[:, 0], ylabel='height/mm')
  _im = None
  for idx, ax in enumerate(axis):
    ax[0].imshow(y[idx], vmin=vmin, vmax=vmax, extent=extent, cmap=cmap)
    _im = ax[1].imshow(y_pred[idx], vmin=vmin, vmax=vmax, extent=extent, cmap=cmap)

  # spacing between subplots
  # plt.subplots_adjust(right=.6, left=0.15)
  plt.subplots_adjust(right=.77, left=0.23)


  # add legend
  # values = np.unique(y[0].ravel())
  # colors = [_im.cmap(_im.norm(value)) for value in values]
  # patches = [mpatches.Patch(color=colors[i], label="{l}".format(l=labels[i])) for i in range(len(values))]
  # # plt.legend(handles=patches, bbox_to_anchor=(-.77, 5.5), loc=2, borderaxespad=0.)
  # # plt.legend(handles=patches, loc=0, borderaxespad=0.)
  # fig.legend(handles=patches, loc=10, borderaxespad=0)

  filename = 'all_ground_truths_vs_predictions.svg'
  plt.savefig(os.path.join(output_dir, filename), bbox_inches='tight', dpi=300)
  plt.close()


def plot_scatter_plot(y, y_pred, output_dir=None, sampling_rate=1000):
  # sns.scatterplot(y, y_pred)
  y = y.reshape(-1)
  y_pred = y_pred.reshape(-1)
  y = np.around(y, decimals=1)
  y_pred = np.around(y_pred, decimals=1)
  sort_indices = np.argsort(y)
  y = y[sort_indices]
  y_pred = y_pred[sort_indices]
  y = y[0::sampling_rate]
  y_pred = y_pred[0::sampling_rate]
  plt.scatter(y, y_pred)
  plt.xlabel('true thickness/mm')
  plt.ylabel('estimated thickness/mm')
  # plt.axis('equal')
  # plt.axis('square')
  # plt.xlim([0, plt.xlim()[1]])
  # plt.ylim([0, plt.ylim()[1]])

  save_or_show_plot(output_dir, filename='scatter_plot_sr_%s.svg' % sampling_rate)


def plot_binwise_boxplots(bin_to_abs_diff, sampling_rate=1000, output_dir=None):
  """
  :param dict[int] bin_to_abs_diff:
  :param int sampling_rate:
  :param str output_dir:
  :return:
  """
  _min = -1
  _max = 0
  for b in sorted(bin_to_abs_diff.keys()):
    values = bin_to_abs_diff[b]
    _min = min(_min, values.min())
    _max = max(_max, values.max())
    sampled_values = values[0::sampling_rate]
    plt.boxplot(sampled_values, positions=[b])

  plt.ylim([_min, _max])
  plt.xlabel('thickness/mm')
  plt.ylabel('absolute deviation/mm')
  save_or_show_plot(output_dir, filename='absolute_binwise_thickness_deviation.svg')


def save_or_show_plot(output_dir=None, prefix=None, filename='', transparent=False):
  """
  Saves the current plot in output_dir with as 'prefix_filename.png'
  :param str|None output_dir:
  :param None|int|str prefix:
  :param str filename:
  :param bool transparent:
  :return:
  """

  FILE_EXTs = ['png', 'svg']
  ext = filename.split('.')[-1]
  if ext not in FILE_EXTs:
    filename += ('.' + FILE_EXTs[0])
  if prefix is not None:
    prefix = str(prefix)
    if prefix != '':
      prefix = '%s' % prefix.zfill(3)
    filename = '%s_%s' % (prefix, filename)

  if output_dir is not None:
    assert isinstance(filename, str) and len(filename) > 0
    os.makedirs(output_dir, exist_ok=True)
    # # set readable font size
    # # plt.style.use('default')
    # matplotlib.rcParams.update({
    #   # 'pgf.texsystem': 'pdflatex',
    #   'font.family': 'serif',
    #   'font.size': 12,
    #   'axes.labelsize': 12,
    #   # 'text.usetex': True,
    #   'legend.fontsize': 10,
    #   'xtick.labelsize': 10,
    #   'ytick.labelsize': 10,
    #   # 'pgf.rcfonts': False,
    #   # 'pdf.fonttype': 42
    # })
    plt.savefig(os.path.join(output_dir, filename), transparent=transparent, bbox_inches='tight')
    plt.close()
  else:
    plt.show()


def set_pgf_size(width, fraction=1):
  # Width of figure
  fig_width_pt = width * fraction

  # Convert from pt to inches
  inches_per_pt = 1 / 72.27

  # Golden ratio to set aesthetic figure height
  golden_ratio = (5**.5 - 1) / 2

  # Figure width in inches
  fig_width_in = fig_width_pt * inches_per_pt
  # Figure height in inches
  fig_height_in = fig_width_in * golden_ratio

  fig_dim = (fig_width_in, fig_height_in)

  return fig_dim
