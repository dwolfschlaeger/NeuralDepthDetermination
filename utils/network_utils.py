import numbers

layer_idx_to_activation = {
  1: 'tanh',
  0: 'relu'
}


def add_dropout(result, src, dropout):
  pass


def add_reshape_temporal(result, input_shape):
  layer_name = 'reshape'
  # Flatten first
  if isinstance(input_shape, int):
    result[layer_name] = {'class': 'Reshape', 'target_shape': (input_shape, 1)}
  elif isinstance(input_shape, (tuple, list)):
    assert len(input_shape) == 3
    result[layer_name] = {'class': 'Reshape', 'target_shape': (input_shape[0] * input_shape[1], input_shape[2])}

  return layer_name


def generate_ff_network(result, num_layers=1, num_units=100, dropout=None):
  """
  Adds a feed forward network topology to result
  :param dict result:
  :param int num_layers:
  :param int num_units:
  :param None|float dropout:
  :return:
  """
  assert result is not None
  layer_name = 'flatten'
  result[layer_name] = {'class': 'Flatten'}
  src = layer_name

  for i in range(1, num_layers + 1):
    layer_name = 'h%d' % i
    result[layer_name] = {'class': 'Dense', 'units': num_units, 'activation': layer_idx_to_activation[i % 2], 'from': src}
    src = layer_name
    if dropout is not None and isinstance(dropout, numbers.Number):
      layer_name = 'd%d' % i
      result[layer_name] = {'class': 'Dropout', 'rate': dropout, 'from': src}
      src = layer_name

  return layer_name


def generate_simple_rnn(result, input_shape, num_layers, num_units, dropout=None):
  """
  Adds a recurrent neural network topology to result
  :param dict result:
  :param int|tuple|list input_shape:
  :param int num_layers:
  :param int num_units:
  :param None|numbers.Number dropout:
  :return:
  """
  assert result is not None
  src = add_reshape_temporal(result, input_shape)
  layer_name = None
  for i in range(1, num_layers + 1):
    layer_name = 'simple_rnn%d' % i
    result[layer_name] = {'class': 'SimpleRNN', 'units': num_units, 'return_sequences': True, 'from': src}
    if dropout is not None and isinstance(dropout, numbers.Number):
      result[layer_name]['dropout'] = dropout
    src = layer_name
    # last hidden layer does not return full sequence
    if i == num_layers:
      result[layer_name]['return_sequences'] = False

  return layer_name


def generate_rnn_network(result, input_shape, num_layers, num_units, dropout=None):
  """
  Adds a recurrent neural network topology to result
  :param dict result:
  :param int|list|tuple input_shape:
  :param int num_layers:
  :param int num_units:
  :param None|numbers.Number dropout:
  :return:
  """
  assert result is not None
  src = add_reshape_temporal(result, input_shape)
  layer_name = None
  for i in range(1, num_layers + 1):
    layer_name = 'rnn%d' % i
    result[layer_name] = {'class': 'RNN', 'units': num_units, 'return_sequences': True, 'from': src}
    if dropout is not None and isinstance(dropout, numbers.Number):
      result[layer_name]['dropout'] = dropout
    src = layer_name
    # last hidden layer does not return full sequence
    if i == num_layers:
      result[layer_name]['return_sequences'] = False

  return layer_name


def generate_lstm_network(result, input_shape, num_layers, num_units, dropout=None):
  assert result is not None
  src = add_reshape_temporal(result, input_shape)
  layer_name = None
  for i in range(1, num_layers + 1):
    layer_name = 'lstm%d' % i
    result[layer_name] = {'class': 'LSTM', 'units': num_units, 'return_sequences': True, 'from': src}
    if dropout is not None and isinstance(dropout, numbers.Number):
      result[layer_name]['dropout'] = dropout
    src = layer_name
    # last hidden layer does not return full sequence
    if i == num_layers:
      result[layer_name]['return_sequences'] = False

  return layer_name


def generate_blstm_network(result, input_shape, num_layers, num_units, dropout=None):
  # TODO
  assert result is not None
  src = add_reshape_temporal(result, input_shape)
  layer_name = None
  for i in range(1, num_layers + 1):
    layer_name = 'simple_rnn%d' % i
    result[layer_name] = {'class': 'LSTM', 'units': num_units, 'return_sequences': True, 'from': src}
    if dropout is not None and isinstance(dropout, numbers.Number):
      result[layer_name]['dropout'] = dropout
    src = layer_name
    # last hidden layer does not return full sequence
    if i == num_layers:
      result[layer_name]['return_sequences'] = False

  return layer_name


def generate_conv1d(result, input_shape, num_layers, num_units, dropout=None):
  """
  Performs a temporal convolution
  :param dict result:
  :param int|list|tuple input_shape:
  :param int num_layers:
  :param int num_units:
  :param None|numbers.Number dropout:
  :return:
  """
  assert result is not None
  src = add_reshape_temporal(result, input_shape)

  for i in range(1, num_layers + 1):
    layer_name = 'conv1d_%d' % i
    result[layer_name] = {'class': 'Conv1D', 'filters': num_units//4, 'kernel_size': 3,  'padding': 'same', 'from': src}
    src = layer_name
    if dropout is not None and isinstance(dropout, numbers.Number):
      layer_name = 'd%d' % i
      result[layer_name] = {'class': 'Dropout', 'rate': dropout, 'from': src}
      src = layer_name

  layer_name = 'flatten'
  result[layer_name] = {'class': 'Flatten', 'from': src}

  return layer_name


def generate_conv2d(result, input_shape, num_layers, num_units, dropout=None):
  assert result is not None
  pool_size = (4, 4) if num_layers <= 2 else (2, 2)
  H = 512
  W = 640
  # src = add_reshape_temporal(result, input_shape)
  src = None

  for i in range(1, num_layers + 1):
    layer_name = 'conv2d_%d' % i
    result[layer_name] = {'class': 'Conv2D', 'filters': num_units // 4, 'kernel_size': 3, 'padding': 'same', 'activation': 'relu'}
    if src is not None:
      result[layer_name]['from'] = src
    src = layer_name
    layer_name = 'maxpooling_%d' % i
    result[layer_name] = {'class': 'MaxPool2D', 'pool_size': pool_size, 'from': src}
    src = layer_name

    if dropout is not None and isinstance(dropout, numbers.Number):
      layer_name = 'd%d' % i
      result[layer_name] = {'class': 'Dropout', 'rate': dropout, 'from': src}
      src = layer_name

  layer_name = 'flatten'
  result[layer_name] = {'class': 'Flatten', 'from': src}

  return layer_name



def generate_network(network_type, input_shape, num_layers=1, num_units=100, output_units=1, dropout=None,
                     output_activation='relu'):
  result = {}
  src = None
  if network_type == 'ff':
    src = generate_ff_network(result, num_layers, num_units, dropout)
  elif network_type == 'simple_rnn':
    src = generate_simple_rnn(result, input_shape, num_layers, num_units, dropout)
  elif network_type == 'rnn':
    src = generate_rnn_network(result, input_shape, num_layers, num_units, dropout)
  elif network_type == 'lstm':
    src = generate_lstm_network(result, input_shape, num_layers, num_units, dropout)
  elif network_type == 'conv1d':
    src = generate_conv1d(result, input_shape, num_layers, num_units, dropout)
  elif network_type == 'conv2d':
    src = generate_conv2d(result, input_shape, num_layers, num_units, dropout)
  else:
    raise NotImplementedError('Network type: "%s" not implemented' % network_type)

  # take input from hidden layer
  if src is not None:
    result['output'] = {'class': 'Dense', 'units': output_units, 'activation': output_activation, 'from': src}
  # single output layer
  else:
    result['flatten'] = {'class': 'Flatten'}
    result['output'] = {'class': 'Dense', 'units': output_units, 'activation': output_activation, 'from': 'flatten'}

  return result
