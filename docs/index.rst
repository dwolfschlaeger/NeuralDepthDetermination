.. NeuralDepthDetermination documentation master file, created by
   sphinx-quickstart on Wed Feb  5 14:42:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NeuralDepthDetermination's documentation!
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
