#!/usr/bin/env python
import argparse
import os

import numpy as np

from Dataset import Dataset
from utils import plot_utils
import global_settings as gs

DATASET_TYPE_OPTIONS = ['all', 'train', 'test', 'val']
DATASET_TYPE         = DATASET_TYPE_OPTIONS[0]
OUTPUT_DIR           = 'analyse_data_plots'
SHOW_PLOTS           = False
SHUFFLE_SPECIMENS    = [0, 3, 4, 5, 7, 9, 10, 12, 14, 15, 6, 8, 13, 1, 2, 11, 16]


def get_dataset_type(dataset):
  assert DATASET_TYPE in DATASET_TYPE_OPTIONS
  if DATASET_TYPE == 'all':
    return dataset.original_data
  elif DATASET_TYPE == 'train':
    return dataset.train_data
  elif DATASET_TYPE == 'test':
    return dataset.test_data
  elif DATASET_TYPE == 'val':
    return dataset.val_data


def plot_inputs_outputs(X, Y, specimens, Y_labels=None):
  """
  Plots inputs and outputs for the given dataset
  :param np.array_like X:
  :param np.array_like Y:
  :param np.array_like specimens:
  :param None|np.array_like Y_labels:
  :return:
  """
  output_dir = None
  if OUTPUT_DIR is not None and not SHOW_PLOTS:
    output_dir = os.path.join(OUTPUT_DIR, '%s_inputs_outputs' % DATASET_TYPE)
    print('Saving plots in: %s' % output_dir)

  for i, specimen in enumerate(specimens):
    if Y_labels is not None:
      plot_utils.plot_input_target_and_output(X[i], Y[i], Y_labels[i], None, specimen, output_dir, i)
    else:
      plot_utils.plot_input_target_and_output(X[i], Y[i], None, None, specimen, output_dir, i)


def compute_histogram(X, Y, specimens, specimen_wise=False):
  output_dir = None
  if OUTPUT_DIR is not None and not SHOW_PLOTS:
    if specimen_wise:
      output_dir = os.path.join(OUTPUT_DIR, '%s_histograms_specimen_wise' % DATASET_TYPE)
    else:
      output_dir = os.path.join(OUTPUT_DIR, '%s_histograms' % DATASET_TYPE)
    print('Saving plots in: %s' % output_dir)

  if specimen_wise:
    pass
  else:
    bins = np.arange(0, 257)
    x_ticks = np.arange(0, 257, 20)
    plot_utils.plot_histogram(X, bins, x_ticks, title='Input Histogram', output_dir=output_dir, x_label='Gray values')
    bins = np.arange(0, 10, 0.5)
    x_ticks = np.arange(0, 10)
    plot_utils.plot_histogram(Y, bins, x_ticks, x_label='Thickness/mm', title='Target Histogram', output_dir=output_dir)
    bins = np.arange(0, 5)
    x_ticks = np.arange(0, 5)
    # plot_utils.plot_histogram(Y_labels, bins, x_ticks, x_label='Classes', title='Target Label Histogram', output_dir=output_dir)


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Performs some analysis options for the given dataset',
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)

  # positional arguments
  parser.add_argument('dataset_path', type=str,
                      help='Path to the dataset')
  # optional arguments for dataset
  parser.add_argument('--dataset_type', type=str, default=DATASET_TYPE_OPTIONS[0], choices=DATASET_TYPE_OPTIONS,
                      help='Specifies the concrete dataset')
  parser.add_argument('--test_split', type=float, default=0.3,
                      help='Specifies the concrete dataset')
  parser.add_argument('--val_split', type=float, default=0.3,
                      help='Specifies the concrete dataset')
  parser.add_argument('--input_shape', type=int, default=gs.C, nargs='+',
                      help='Shape of the input data')
  parser.add_argument('--dim_reduction')
  # parser.add_argument('--shuffle_specimens', default='random')
  parser.add_argument('--shuffle_specimens', default=None)
  parser.add_argument('--shuffle_pixels')
  parser.add_argument('--normalization')
  # optional arguments for analysis
  parser.add_argument('-show', '--show_plots', action='store_true',
                      help='Shows plots interactively, otherwise saved in output_dir')
  parser.add_argument('-dir', '--output_dir', type=str, default=OUTPUT_DIR,
                      help='Output directory. Only considered if -show is not set')
  parser.add_argument('--plot_inputs_outputs', action='store_true',
                      help='Plots the inputs and outputs given the input_shape')
  parser.add_argument('--compute_histogram', action='store_true',
                      help='Computes the histogram for all inputs, targets and target labels')
  parser.add_argument('--compute_histogram_specimen', action='store_true',
                      help='Computes the histogram for inputs, targets and target labels of each specimen')
  parser.add_argument('--plot_inputs_outputs_accuracy', action='store_true',
                      help='Plots the difference between input and target mask')


  # get passed arguments for dataset initialization
  args = parser.parse_args()
  print('args', args)
  dataset_params = {
    'file_path'         : args.dataset_path,
    'test_split'        : args.test_split,
    'val_split'         : args.val_split,
    'input_shape'       : args.input_shape,
    'dim_reduction'     : args.dim_reduction,
    'shuffle_specimens' : args.shuffle_specimens,
    'shuffle_pixels'    : args.shuffle_pixels,
    'normalization'     : args.normalization
  }
  DATASET_TYPE = args.dataset_type

  # load dataset
  dataset = Dataset()
  if isinstance(dataset_params['input_shape'], list) and len(dataset_params['input_shape']) == 1:
    dataset_params['input_shape'] = dataset_params['input_shape'][0]
  dataset.init_dataset_from_file(**dataset_params)
  print(dataset)
  X, Y = get_dataset_type(dataset)
  specimens     = dataset.specimen_names

  # get passed arguments run dataset inspections
  SHOW_PLOTS   = args.show_plots
  OUTPUT_DIR   = args.output_dir

  if args.plot_inputs_outputs:
    plot_inputs_outputs(X, Y, specimens, None)

  if args.compute_histogram:
    compute_histogram(X, Y, specimens)

  if args.compute_histogram_specimen:
    compute_histogram(X, Y, specimens, specimen_wise=True)

  print('Done')
