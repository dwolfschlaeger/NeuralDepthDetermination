import os
import inspect
import sys
import unittest
import tempfile
import shutil

CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)
from Config import ConfigGenerator
from Label import Label


class TestLabelConfigs(unittest.TestCase):

  def setUp(self):
    self.root_dir = tempfile.mkdtemp()
    self.config_generator = ConfigGenerator(self.root_dir, output_dir='data', config_dir='configs')
    print('Root dir:', self.root_dir)

  def tearDown(self):
    # shutil.rmtree(self.root_dir)
    pass

  def test_label(self):
    configs = self.config_generator.generate_label_configs()

    for config in configs:
      Label(config)


if __name__ == '__main__':
  unittest.main()
