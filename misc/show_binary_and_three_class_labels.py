s_idx = 0

y_binary = self.Y.copy()
y_binary[y_binary > mean] = 0
y_binary[(y_binary <= mean) & (y_binary > 0)] = 1
_, height, width = y_binary.shape
extent = (0, width, 0, height)
x_ticks = [0, width]
y_ticks = [0, height]
y_three = self.Y.copy()
y_three[y_three <= 0] = 0
y_three[(y_three > 0) & (y_three <= 4)] = 1
y_three[(y_three >40)] = 2

import matplotlib
matplotlib.rcParams.update({
  # 'pgf.texsystem': 'pdflatex',
  'font.family': 'serif',
  'font.size': 12,
  'axes.labelsize': 12,
  # 'text.usetex': True,
  'legend.fontsize': 10,
  'xtick.labelsize': 10,
  'ytick.labelsize': 10,
  # 'pgf.rcfonts': False,
  # 'pdf.fonttype': 42
})
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm

norm = mpl.colors.Normalize(vmin=0, vmax=2)
cmap = cm.brg
# # cmap = cm.gist_heat
# # cmap = cm.Set3

plt.figure(1)
plt.subplot(121)
plt.ylabel('pixel')
plt.xlabel('pixel')
plt.xticks(x_ticks)
plt.yticks(y_ticks)
plt.imshow(y_binary[s_idx], cmap=cmap, vmin=0, vmax=2, extent=extent)
#
plt.subplot(122)
plt.xlabel('pixel')
plt.imshow(y_three[s_idx], cmap=cmap, vmin=0, vmax=2, extent=extent)
# plt.imshow(self.Y[0])
plt.xticks(x_ticks)
plt.yticks(y_ticks)
plt.tight_layout()

plt.show()
