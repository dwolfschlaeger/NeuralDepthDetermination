$specimen_dirs = @("111", "112", "113", "114", "121", "122", "123", "124", "131", "132", "133", "134")
$specimen_types = @("a", "b")
$sub_dirs = @("auto_histogram", "auto_histogram_with_scale", "full_histogram", "phase")

$work_dir = Get-Location
Write-Host $work_dir
Write-Host "Start creating directories..."
For ($i = 0; $i -lt $specimen_dirs.Length; $i++) {
    For ($j = 0; $j -lt $specimen_types.Length; $j++) {
        For ($k = 0; $k -lt $sub_dirs.Length; $k++) {
            $dir = -join($specimen_dirs[$i], $specimen_types[$j])
            $dir = Join-Path -Path $work_dir -ChildPath $dir
            $dir = Join-Path -Path $dir -ChildPath $sub_dirs[$k]
            New-Item -type directory -path $dir -force | out-null
        }
    }
}

Write-Host "Done"
Start-SLeep -s 20
