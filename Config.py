import os
import json
from collections import OrderedDict


class Config:
  """
  Provides task specific parameters which are read from a *.config file
  """

  def __init__(self, filename, params=None):
    self.filename = filename
    if params is None:
      self._params = {}
    else:
      self._params = params

  def __str__(self):
    return json.dumps(self.params, indent=2)

  def __repr__(self):
    return json.dumps(self.params, indent=2)

  def __eq__(self, other):
    return self.filename == other.filename and self.params == other.params

  def __ne__(self, other):
    return not self.__eq__(other)

  @property
  def params(self):
    return self._params

  @params.setter
  def params(self, params):
    assert isinstance(params, dict)
    for k, v in params.items():
      if k not in self._params:
        self._params[k] = v

  @params.deleter
  def params(self):
    del self._params

  def value(self, key, default=None):
    if key in self.params:
      if self.params[key] is not None:
        return self.params[key]

    return default

  @classmethod
  def from_file(cls, f):
    """
    Creates a config from file
    :param str f: filename
    :return: config
    :rtype: Config
    """
    assert os.path.isfile(f), 'config file not found: %r' % f

    content = open(f, 'r').read()
    kwargs = {}
    co = compile(content, f, 'exec')
    eval(co, None, kwargs)
    filename = os.path.split(f)[-1]
    return Config(filename, kwargs)


class ConfigGenerator:
  """
  Generates configs and writes them to file
  """

  def __init__(self, root_dir, output_dir, config_dir='configs', max_num_configs=None):
    os.makedirs(output_dir, exist_ok=True)
    self.root_dir = root_dir
    output_dir = os.path.join(root_dir, output_dir)
    os.makedirs(output_dir, exist_ok=True)
    self.output_dir = output_dir
    self.config_dir = None
    if config_dir is not None:
      config_dir = os.path.join(root_dir, config_dir)
      os.makedirs(config_dir, exist_ok=True)
      self.config_dir = config_dir

    self.max_num_configs = max_num_configs

  def generate_segment_configs(self):
    configs = []
    task = 'segment'
    params = {
      'task': task
    }

    return configs

  def generate_label_configs(self):
    """
    Generates some label configs
    :return:
    """
    task = 'label'
    params = {
      'task': task,
      'data_dir': None,
      'image_type': None,
      'output_type': 'pickle',
      'class_to_thickness': [
        ('background', 0),
        ('severe', 2),
        ('significant', 4),
        'safe'
      ]
    }

    data_dirs = ['data/raw_640x512', 'data/raw_small_640x512']
    img_types = ['auto_histogram', 'full_histogram', 'phase']

    conf_idx = 0
    configs = []
    for data_dir in data_dirs:
      params['data_dir'] = data_dir
      for img_type in img_types:
        params['image_type'] = img_type

        filename = '%s_%s_%s_%s.config' % (str(conf_idx).zfill(4), task, data_dir.split('/')[-1], img_type)

        params['filename'] = filename
        params['output_dir'] = self.output_dir
        config = self.create_single_config(filename, params.copy())
        configs.append(config)
        conf_idx += 1
        self.check_num_configs(conf_idx)

    return configs

  def generate_train_configs(self, data_path_prefix='data/20-02-09_3_specimens', num_epochs=500, num_layers=None,
                             num_units=None, network_types=None, input_shapes=None, shuffle_pixels=None,
                             img_types=None, normalizations=None, dropout_rates=None, test_split=0.3, val_split=0.3,
                             shuffle_specimens=None, losses=['mae'], output_activation='relu', num_output_units=1,
                             class_to_thickness=None):
    """
    Generates multiple train configs
    :return:
    """
    from utils.network_utils import generate_network
    task = 'train'

    DEFAULT_BATCH_SIZE = 2048
    data_path_prefix = data_path_prefix
    if not isinstance(num_epochs, int):
      num_epochs = 500
    if num_layers is None:
      num_layers = [2]
    if num_units is None:
      num_units = [50]
    if network_types is None:
      network_types = ['ff', 'simple_rnn', 'lstm', 'conv1d', 'conv2d']
    if input_shapes is None:
      # input_shapes = [21, 3, (512, 512, 21), (512, 512, 3)]
      input_shapes = [21, (512, 640, 21)]
    # if shuffle_pixels is None:
    #   shuffle_pixels = [True, False]
    if img_types is None:
      img_types = ['auto_histogram', 'full_scale', 'phase']
    if normalizations is None:
      # normalizations = [None, 'zero-one', 'mean', 'mean-variance']
      normalizations = [None]
    if dropout_rates is None:
      dropout_rates = [None]
    if shuffle_specimens is None:
      shuffle_specimens = 'random'
    params = {
      'filename': '',
      'task': task,
      'data_path': None,
      'shuffle_specimens': shuffle_specimens,
      'use_data_augmentation': True,
      'test_split': test_split,
      'val_split': val_split,
      'num_epochs': num_epochs,
      'loss': losses[0],
      'optimizer': 'nadam',
      'metrics': ['mae', 'accuracy'],
      'input_shape': None,
      'output_shape': 1,
      'batch_size': DEFAULT_BATCH_SIZE,
      'network': None,
      'use_early_stopping': True,
      'stopping_patience': 10,
      'stopping_mode': 'min',
      'output_dir': None
    }
    if class_to_thickness is not None:
      params['class_to_thickness'] = [
        ('background', 0),
        ('defect', 4),
        'sound'
      ]

    configs = []
    for network_type in network_types:
      conf_idx = 0
      for input_shape in input_shapes:
        params['input_shape'] = input_shape
        if isinstance(input_shape, int):
          if network_type in ['conv2d']:
            continue
          input_shape_str = str(input_shape)
        elif isinstance(input_shape, (tuple, list)):
          if network_type in ['rnn', 'conv1d', 'lstm', 'simple_rnn']:
            continue
          input_shape_str = [str(x) for x in input_shape]
          input_shape_str = 'x'.join(input_shape_str)
          if len(input_shape) == 1:
            input_shape_str += 'x'
        else:
          raise Exception('Input shape: %s not supported' % input_shape)
        if isinstance(input_shape, int):
          shuffle_pixel = True
          params['batch_size'] = DEFAULT_BATCH_SIZE
        else:
          shuffle_pixel = False
          params['batch_size'] = 10
        params['shuffle_pixels'] = shuffle_pixel
        for nl in num_layers:
          for nu in num_units:
            for do in dropout_rates:
              if isinstance(input_shape, (tuple, list)):
                num_output_units = input_shape[0] * input_shape[1]
              network = generate_network(network_type, input_shape, nl, nu, num_output_units, do, output_activation)
              params['network'] = network
              for img_type in img_types:
                params['data_path'] = data_path_prefix + '_%s.pickle' % img_type
                for norm in normalizations:
                  params['normalization'] = norm
                  for loss in losses:
                    params['loss'] = loss
                    if len(losses) > 1:
                      filename = '%s_%s_%s_%dx%d_Do-%s_Input-Shape-%s_Shuffle-Pixels-%s_Img-Type-%s_Norm-%s_Loss-%s.config' % \
                                 (task, network_type.upper(), str(conf_idx).zfill(4), nl, nu, do,
                                  input_shape_str, shuffle_pixel, img_type, norm, loss)
                    # single loss
                    else:
                      filename = '%s_%s_%s_%dx%d_Do-%s_Input-Shape-%s_Shuffle-Pixels-%s_Img-Type-%s_Norm-%s.config' % \
                                 (task, network_type.upper(), str(conf_idx).zfill(4), nl, nu, do,
                                  input_shape_str, shuffle_pixel, img_type, norm)

                    params['filename'] = filename
                    params['output_dir'] = os.path.join(self.output_dir, filename)
                    config = self.create_single_config(filename, params.copy())
                    configs.append(config)
                    conf_idx += 1
                    self.check_num_configs(conf_idx)

    return configs


  def generate_evaluate_configs(self):
    task = 'evaluate'
    params = {
      'task': task
    }

  def check_num_configs(self, num_configs):
    if self.max_num_configs is not None and num_configs >= self.max_num_configs:
      return

  def create_single_config(self, filename, params, write_to_file=True):
    """
    Creates a config
    :param str filename:
    :param dict[str] params:
    :param bool write_to_file:
    :return: Config.Config
    """
    config = Config(filename, params)
    if write_to_file and self.config_dir is not None:
      self.write_config_to_file(config)

    return config

  def write_config_to_file(self, config):
    """
    Writes config to file
    :param Config.Config config:
    :return: None
    """
    filename = config.value('filename', None)
    assert filename is not None

    file_params = ''
    for key, value in config.params.items():
      if isinstance(value, str):
        file_params += key + ' = \'' + value + '\'\n'
      else:
        file_params += key + ' = ' + str(value) + '\n'

    f_path = os.path.join(self.config_dir, filename)
    f = open(f_path, 'w')
    f.write(file_params)
    f.close()
