import os
import pickle
import logging

import global_settings as gs
from utils.io_utils import aggregate_raw_image_data
from utils.img_utils import estimate_wall_thicknesses, align_wall_thicknesses_to_phase_imgs, create_labels
from DataReader import DataReader

logger = logging.getLogger(gs.LOGGER_NAME)


class Evaluate:
  def __init__(self, config):
    pass
    # config parameters
