#!/usr/bin/env python3

import sys
import os
import logging
import typing
import time

from Config import Config
from Log import init_logger

config     = None # type: typing.Optional[Config]
logger     = None
task       = None
train_data = None
val_data   = None
test_data  = None


def init_config(config_filename):
  """
  Initializes the config.
  :param str config_filename:
  :return:
  """
  global config, logger, task, train_data, val_data, test_data
  config = Config.from_file(config_filename)
  task   = config.value('task', 'train')
  logger = init_logger(config)


def run_task():
  """
  Runs the given task.
  """
  logger.info('Starting task: %s' % task)
  if task == 'label':
    from Label import Label
    Label(config)
  elif task == 'train':
    from Train import Train
    Train(config)
  elif task == 'evaluate':
    from Evaluate import Evaluate
    Evaluate(config)
  elif task == 'serve':
    from Serve import Serve
    Serve(config)
  else:
    logger.warn('Task: %s is not supported' % task)


def main(argv):
  """
  Main entry point which takes exactly one argument namely the configuration file.
  :param argv:
  :return:
  """

  start = time.time()
  # logger.debug('Passed arguments %s' % argv)
  print('Passed arguments %s' % argv)
  assert len(argv) >= 2, 'usage: %s <config>' % argv[0]
  init_config(argv[1])
  run_task()
  end = time.time()
  elapsed = end - start
  logger.info('Elapsed time: %d seconds' % elapsed)


if __name__ == '__main__':
  main(sys.argv)
