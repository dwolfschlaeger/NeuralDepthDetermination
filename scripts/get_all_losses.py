#!/usr/bin/env python
import sys
import glob
import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

DEFAULT_OUTPUT_FILE = 'loss.pickle'
LOSS_TYPES = ['mae', 'mse', 'msle', 'cosine_similarity', 'logcosh', 'huber']
losses = { loss_type: {} for loss_type in LOSS_TYPES}

def get_loss_from_log_file(file_path):
  pattern = 'Loss:'
  f = open(file_path, 'r')
  content = f.readlines()
  no_hit = True
  for line in content:
    if pattern in line:
      no_hit = False
      # print(line)
      loss = line.split(pattern)[-1].strip()
      if loss is None:
        loss = -1
      loss = float(loss)
      return loss
  if no_hit:
    print('Loss not found: %s' % file_path)
    return -1.0


header = '{:16} {:16} {:16} {:16} {:16} {:16}'.format('Loss', 'Min', 'Max', 'Median', 'Deviation', 'Model Topology')
print(header)
print('=' * len(header))
# check if recall was already computed
if os.path.isfile(DEFAULT_OUTPUT_FILE):
  with open(DEFAULT_OUTPUT_FILE, 'rb') as f:
    losses = pickle.load(f)
else:
  for loss_type in LOSS_TYPES:
    _losses = []
    _models = []
    log_files = glob.glob('./train*_Loss-' + loss_type + '.config.*')
    for log_file in log_files:
      loss = get_loss_from_log_file(log_file)
      _losses.append(loss)
      _models.append(log_file)
      
    _losses = np.asarray(_losses)
    _models = np.asarray(_models)
    sort_indices = np.argsort(_losses)
    _losses = _losses[sort_indices]
    _models = _models[sort_indices]

    losses[loss_type] = {}
    losses[loss_type]['losses'] = _losses
    losses[loss_type]['models'] = _models
  with open(DEFAULT_OUTPUT_FILE, 'wb') as outfile:
    pickle.dump(losses, outfile)


# print(losses)
best_models = []
for loss_type in LOSS_TYPES:
  _losses = losses[loss_type]['losses']
  best_models.append(losses[loss_type]['models'][0])
  model_topology = losses[loss_type]['models'][0].split('_')[3]
  print('{:<16} {:<16.2f} {:<16.2f} {:<16.2f} {:<16.2f} {:>16}'.format(loss_type, _losses.min(), np.median(_losses), _losses.max(), np.std(_losses), model_topology))
  # print('=' * len(header))

for m in best_models:
  print(m)

# def set_box_color(bp, color):
#     plt.setp(bp['boxes'], color=color)
#     plt.setp(bp['whiskers'], color=color)
#     plt.setp(bp['whiskers'], linestyle='--', linewidth=1)
#     plt.setp(bp['caps'], color=color)
#     plt.setp(bp['medians'], color=color)

# # prepare data for plotting
# none_recalls = []
# zero_one_recalls = []
# mean_recalls = []
# mean_var_recalls = []

# for i, data_type in enumerate(DATA_TYPES):  
#   for j, norm in enumerate(NORM_TYPES):
#     _r = recalls[data_type][norm]['recalls']
#     if norm == NORM_TYPES[0]:
#       none_recalls.append(_r)
#     elif norm == NORM_TYPES[1]:
#       zero_one_recalls.append(_r)
#     elif norm == NORM_TYPES[2]:
#       mean_recalls.append(_r)
#     elif norm == NORM_TYPES[3]:
#       mean_var_recalls.append(_r)



# plt.figure()
# num_recalls = len(none_recalls)
# bpl0 = plt.boxplot(none_recalls, positions=np.array(range(num_recalls))*2.0-0.4, widths=0.2)
# set_box_color(bpl0, 'red')
# bpl1 = plt.boxplot(zero_one_recalls, positions=np.array(range(num_recalls))*2.0-0.1,widths=0.2)
# set_box_color(bpl1, 'green')
# bpl2 = plt.boxplot(mean_recalls, positions=np.array(range(num_recalls))*2.0+0.2, widths=0.2)
# set_box_color(bpl2, 'blue')
# bpl3 = plt.boxplot(mean_var_recalls, positions=np.array(range(num_recalls))*2.0+0.5, widths=0.2)
# set_box_color(bpl3, 'gray')

# fs = 16
# plt.rcParams.update({'font.size': fs})
# plt.plot([], c='red', label='None')
# plt.plot([], c='green', label='Zero-One')
# plt.plot([], c='blue', label='Mean')
# plt.plot([], c='gray', label='Mean-Variance')
# plt.legend()

# plt.ylabel('Recall/%', fontsize=fs)
# plt.xticks(range(0, len(DATA_TYPES) * len(NORM_TYPES), 2), DATA_TYPES, fontsize=fs)
# # plt.xlim(-2, len(DATA_TYPES) * 2)

# plt.xlim(-0.75,  len(DATA_TYPES) * 1.75)
# plt.ylim(0, 90)
# plt.tight_layout()
# # plt.title(classification_type_to_title[classification_type])
# plt.show()




###### Try pandas
# only_recalls = {}
# num_recalls = 0
# for data_type in DATA_TYPES:
#   only_recalls[data_type] = {}
#   for norm in NORM_TYPES:
#     _r = recalls[data_type][norm]['recalls']
#     only_recalls[data_type][norm] = _r


# norms = 25* [NORM_TYPES[0]] + 25 * [NORM_TYPES[1]] + 25 * [NORM_TYPES[2]] + 25 * [NORM_TYPES[3]]
# print(norms)
# print(len(norms))
# ah = [*only_recalls['auto_histogram']['None'], *only_recalls['auto_histogram']['zero-one'], *only_recalls['auto_histogram']['mean'], *only_recalls['auto_histogram']['mean-var']]
# fs = [*only_recalls['full_scale']['None'], *only_recalls['full_scale']['zero-one'], *only_recalls['full_scale']['mean'], *only_recalls['full_scale']['mean-var']]
# phase = [*only_recalls['phase']['None'], *only_recalls['phase']['zero-one'], *only_recalls['phase']['mean'], *only_recalls['phase']['mean-var']]
# print(len(ah))
# # ah = np.array(only_recalls['auto_histogram']['None'] + only_recalls['auto_histogram']['zero-one'] + only_recalls['auto_histogram']['mean'] + only_recalls['auto_histogram']['mean-var']).reshape(-1)
# # print(len(ah))
# df = pd.DataFrame({'Norm': norms, 'auto_histogram': ah, 'full_scale': fs, 'phase': phase})

# df.boxplot(by='Norm')
# # print(df)
# # dd = pd.melt(df, id_vars=['Norm'], value_vars=DATA_TYPES, var_name='Recall/%')
# # sns.boxplot(x='Norm', y='value', data=dd)



# # NUM_RECALLS = len(only_recalls[data_type][norm])
# # print(NUM_RECALLS)
# # df = pd.DataFrame.from_dict(only_recalls)
# # # df['GROUP'] = pd.Series(l * ['A')]
# # for idx, data_type in enumerate(DATA_TYPES):
# #   df['Group'] = pd.Series(NUM_RECALLS * [idx])


# # print(df)



# # print(df['auto_histogram'])
# # df.boxplot(column=DATA_TYPES)


# # df.boxplot(column=NORM_TYPES[1], by=DATA_TYPES)



# plt.show()
