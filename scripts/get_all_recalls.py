#!/usr/bin/env python
import sys
import glob
import os
import pickle
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


matplotlib.rcParams.update({
  # 'pgf.texsystem': 'pdflatex',
  'font.family': 'serif',
  'font.size': 12,
  'axes.labelsize': 12,
  # 'text.usetex': True,
  # 'legend.fontsize': 10,
  # 'xtick.labelsize': 10,
  # 'ytick.labelsize': 10,
  'legend.fontsize': 12,
  'xtick.labelsize': 12,
  'ytick.labelsize': 12,
  # 'pgf.rcfonts': False,
  # 'pdf.fonttype': 42
})


DEFAULT_OUTPUT_FILE = 'recalls.pickle'
DATA_TYPES = ['auto_histogram', 'full_scale', 'phase']
NORM_TYPES = ['None', 'zero-one', 'mean', 'mean-var']
CLASSIFICATION_TYPES = [2, 3]
assert len(sys.argv) == 2, 'Please provide a classification type "2" for binary "3" for three classes'
classification_type = int(sys.argv[1])
assert classification_type in CLASSIFICATION_TYPES, 'Please provide a classification type "2" for binary "3" for three classes'
classification_type_to_title = {
  2 : 'Binary Classification',
  3 : 'Three-class Classification'
}


if len(sys.argv) == 2:
  data_type = sys.argv[1]
  if data_type in DATA_TYPES:
    DATA_TYPES = [data_type]
  
print('Investigated Data Types: %s' % str(DATA_TYPES))
print('Investigated Norm Types: %s' % str(NORM_TYPES))

recalls = { data_type: {} for data_type in DATA_TYPES for norm in NORM_TYPES}
def get_recall_from_log_file(file_path):
  pattern = 'Recall:'
  f = open(file_path, 'r')
  content = f.readlines()
  for line in content:
    if pattern in line:
      # print(line)
      recall = float(line.split(pattern)[-1].strip()) * 100
      return recall


header = '{:16} {:16} {:16} {:16} {:16} {:16} {:16}'.format('Data Type', 'Normalization', 'Min Recall', 'Median Recall', 'Max Recall', 'Recall Deviation', 'Model Topology')
print(header)
print('=' * len(header))
# check if recall was already computed
if os.path.isfile(DEFAULT_OUTPUT_FILE):
  with open(DEFAULT_OUTPUT_FILE, 'rb') as f:
    recalls = pickle.load(f)
else:
  for data_type in DATA_TYPES:
    for norm in NORM_TYPES:
      _recalls = []
      _models = []
      _norm = norm
      if norm == 'mean':
        _norm = 'mean.conf'

      # log_files = glob.glob('./train*' + data_type + '*Norm-' + _norm + '*')
      log_files = glob.glob('./models/train*' + data_type + '*Norm-' + _norm + '*/train*.log')
      # print('Num log files (%s, %s): %s' % (data_type, _norm, len(log_files)))
      for log_file in log_files:
        recall = get_recall_from_log_file(log_file)
        _recalls.append(recall)
        _models.append(log_file)
      
      _recalls = np.asarray(_recalls)
      _models = np.asarray(_models)
      sort_indices = np.argsort(_recalls)
      _recalls = _recalls[sort_indices]
      _models = _models[sort_indices]


      recalls[data_type][norm] = {}
      recalls[data_type][norm]['recalls'] = _recalls
      recalls[data_type][norm]['models'] = _models
  with open(DEFAULT_OUTPUT_FILE, 'wb') as outfile:
    pickle.dump(recalls, outfile)

for data_type in DATA_TYPES:
  for norm in NORM_TYPES:
    _recalls = recalls[data_type][norm]['recalls']
    model_topology = recalls[data_type][norm]['models'][-1].split('_')[3]
    # print(model_topology)
    print('{:<16} {:<16} {:<16.2f} {:<16.2f} {:<16.2f} {:<16.2f} {:>16}'.format(data_type, norm, _recalls.min(), np.median(_recalls), _recalls.max(), np.std(_recalls), model_topology))
  print('=' * len(header))


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['whiskers'], linestyle='--', linewidth=1)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

# prepare data for plotting
none_recalls = []
zero_one_recalls = []
mean_recalls = []
mean_var_recalls = []

for i, data_type in enumerate(DATA_TYPES):  
  for j, norm in enumerate(NORM_TYPES):
    _r = recalls[data_type][norm]['recalls']
    if norm == NORM_TYPES[0]:
      none_recalls.append(_r)
    elif norm == NORM_TYPES[1]:
      zero_one_recalls.append(_r)
    elif norm == NORM_TYPES[2]:
      mean_recalls.append(_r)
    elif norm == NORM_TYPES[3]:
      mean_var_recalls.append(_r)



plt.figure(figsize=(10,6))
num_recalls = len(none_recalls)
bpl0 = plt.boxplot(none_recalls, positions=np.array(range(num_recalls))*2.0-0.4, widths=0.2)
set_box_color(bpl0, 'red')
bpl1 = plt.boxplot(zero_one_recalls, positions=np.array(range(num_recalls))*2.0-0.1,widths=0.2)
set_box_color(bpl1, 'green')
bpl2 = plt.boxplot(mean_recalls, positions=np.array(range(num_recalls))*2.0+0.2, widths=0.2)
set_box_color(bpl2, 'blue')
bpl3 = plt.boxplot(mean_var_recalls, positions=np.array(range(num_recalls))*2.0+0.5, widths=0.2)
set_box_color(bpl3, 'gray')

fs = 16
#plt.rcParams.update({'font.size': fs})
plt.plot([], c='red', label='None')
plt.plot([], c='green', label='Zero-One')
plt.plot([], c='blue', label='Mean')
plt.plot([], c='gray', label='Mean-Variance')
plt.legend(loc='lower right')

plt.ylabel('recall/%', fontsize=fs)
# plt.xticks(range(0, len(DATA_TYPES) * len(NORM_TYPES), 2), DATA_TYPES, fontsize=fs)
plt.xticks(range(0, len(DATA_TYPES) * len(NORM_TYPES), 2), [dt.replace('_', ' ') for dt in DATA_TYPES])
# plt.xlim(-2, len(DATA_TYPES) * 2)

plt.xlim(-0.75,  len(DATA_TYPES) * 1.75)
plt.ylim(60, 90)
plt.xlabel('input type', fontsize=fs)
plt.tight_layout()

# plt.title(classification_type_to_title[classification_type])
filename = '%s_class_classification.svg' % classification_type
plt.savefig(filename, bbox_inches='tight')
plt.show()






###### Try pandas
# only_recalls = {}
# num_recalls = 0
# for data_type in DATA_TYPES:
#   only_recalls[data_type] = {}
#   for norm in NORM_TYPES:
#     _r = recalls[data_type][norm]['recalls']
#     only_recalls[data_type][norm] = _r


# norms = 25* [NORM_TYPES[0]] + 25 * [NORM_TYPES[1]] + 25 * [NORM_TYPES[2]] + 25 * [NORM_TYPES[3]]
# print(norms)
# print(len(norms))
# ah = [*only_recalls['auto_histogram']['None'], *only_recalls['auto_histogram']['zero-one'], *only_recalls['auto_histogram']['mean'], *only_recalls['auto_histogram']['mean-var']]
# fs = [*only_recalls['full_scale']['None'], *only_recalls['full_scale']['zero-one'], *only_recalls['full_scale']['mean'], *only_recalls['full_scale']['mean-var']]
# phase = [*only_recalls['phase']['None'], *only_recalls['phase']['zero-one'], *only_recalls['phase']['mean'], *only_recalls['phase']['mean-var']]
# print(len(ah))
# # ah = np.array(only_recalls['auto_histogram']['None'] + only_recalls['auto_histogram']['zero-one'] + only_recalls['auto_histogram']['mean'] + only_recalls['auto_histogram']['mean-var']).reshape(-1)
# # print(len(ah))
# df = pd.DataFrame({'Norm': norms, 'auto_histogram': ah, 'full_scale': fs, 'phase': phase})

# df.boxplot(by='Norm')
# # print(df)
# # dd = pd.melt(df, id_vars=['Norm'], value_vars=DATA_TYPES, var_name='Recall/%')
# # sns.boxplot(x='Norm', y='value', data=dd)



# # NUM_RECALLS = len(only_recalls[data_type][norm])
# # print(NUM_RECALLS)
# # df = pd.DataFrame.from_dict(only_recalls)
# # # df['GROUP'] = pd.Series(l * ['A')]
# # for idx, data_type in enumerate(DATA_TYPES):
# #   df['Group'] = pd.Series(NUM_RECALLS * [idx])


# # print(df)



# # print(df['auto_histogram'])
# # df.boxplot(column=DATA_TYPES)


# # df.boxplot(column=NORM_TYPES[1], by=DATA_TYPES)



# plt.show()
