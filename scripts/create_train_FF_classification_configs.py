import os
import inspect
import sys

CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)
from Config import ConfigGenerator
from Train import Train

dataset_prefix = 'data/20-02-09_17_specimens'
num_layers = [2, 4, 6, 8, 10]
num_units = [16, 32, 64, 128, 256]
normalizations = [None, 'zero-one', 'mean', 'mean-variance']
# second argument can be the dataset_prefix
if len(sys.argv) == 2:
  dataset_prefix = sys.argv[1]


def generate_binary_classification_configs():
  output_dir = 'models/binary_classification'
  config_generator = ConfigGenerator(output_dir, 'models', 'configs')
  shuffle_specimens = [0, 3, 4, 5, 7, 9, 10, 12, 14, 15, 6, 8, 13, 1, 2, 11, 16]
  configs = config_generator.generate_train_configs(dataset_prefix, network_types=['ff'], input_shapes=[21],
                                                    dropout_rates=[None], test_split=0.25, val_split=0.25,
                                                    num_layers=num_layers, num_units=num_units,
                                                    shuffle_specimens=shuffle_specimens, normalizations=normalizations,
                                                    losses=['sparse_categorical_crossentropy'],
                                                    output_activation='softmax', num_output_units=2)


# generate_binary_classification_configs()


def generate_three_class_classification_configs():
  class_to_thickness = [
    ('background', 0),
    ('defect', 4),
    'sound-area'
  ]
  output_dir = 'models/three_class_classification'
  config_generator = ConfigGenerator(output_dir, 'models', 'configs')
  shuffle_specimens = [0, 3, 4, 5, 7, 9, 10, 12, 14, 15, 6, 8, 13, 1, 2, 11, 16]
  configs = config_generator.generate_train_configs(dataset_prefix, network_types=['ff'], input_shapes=[21],
                                                    dropout_rates=[None], test_split=0.25, val_split=0.25,
                                                    num_layers=num_layers, num_units=num_units,
                                                    shuffle_specimens=shuffle_specimens, normalizations=normalizations,
                                                    losses=['sparse_categorical_crossentropy'],
                                                    output_activation='softmax', num_output_units=3,
                                                    class_to_thickness=class_to_thickness)


generate_three_class_classification_configs()
