#!/usr/bin/env python
import sys
import numpy as np

assert len(sys.argv) == 2, 'Please provide a text file as a second argument'
file_name = sys.argv[1]
f = open(file_name, 'r')
content = f.readlines()
models = []
scores = []
max_length = 0
for row in content:
  _row = row.split(',')
  model = _row[0]
  score = None
  if len(model) > max_length:
    max_length = len(model)
  # accuracy
  try:
    score = float(_row[2])
  except:
    try:
      print('asdf')
      _l = _row[1].replace('[','').replace(']', '').strip().split(' ')
      _l = [float(i) for i in _l]
      score = _l[1]
    except:
      print('Something went wrong')
  print(_row[1])
  assert score is not None, 'Score is none'
  models.append(model)
  scores.append(score)

print('Number of hits: %s' % len(models))
models = np.asarray(models)
scores = np.asarray(scores)
sort_indices = np.argsort(scores)
scores = scores[sort_indices]
models = models[sort_indices]
for i in range(len(models)):
  # print(models[i], accs[i])
  print(('{:' + str(max_length) + '} {:7.4f}').format(models[i], scores[i]))

