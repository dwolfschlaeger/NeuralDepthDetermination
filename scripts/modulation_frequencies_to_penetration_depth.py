import numpy as np

# thermal diffusivity
alpha = 4.1e-7
# alpha = 4.1 * 10 ** -7
exc_freqs = [0.5, 0.4, 0.3, 0.2, 0.15, 0.125, 0.1, 0.09, 0.08, 0.07, 0.06, 0.05, 0.045, 0.04, 0.035, 0.03, 0.0275, 0.025,
             0.023, 0.02, 0.01, 0.005]

freq_to_depth = lambda freq: np.sqrt(alpha/(np.pi * freq))

for f in exc_freqs:
  print('{:>5} {:6}'.format(f, freq_to_depth(f) * 1000))
