#!/usr/bin/env python
import sys
import glob
import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

DEFAULT_OUTPUT_FILE = 'loss.pickle'
# LOSS_TYPES = ['mae', 'mse', 'msle', 'cosine_similarity', 'logcosh', 'huber']
LOSS_TYPES = ['mae', 'mse', 'logcosh', 'huber']
BINS = [0, 1, 2, 3, 4]
VALUES_PER_BIN = ['Min', 'Median', 'Mean', 'Max', 'Std']

losses = { loss_type: {} for loss_type in LOSS_TYPES}
# for loss in losses:
#   losses[loss]['overall'] = []
#   losses[loss]['models'] = []
#   for b in BINS:
#     losses[loss][b] = []
# print(losses)

def get_loss_and_binwise_loss_from_log_file(file_path):
  overall = bin0 = bin1 = bin2 = bin3 = bin4 = None
  def parse_bin_line(line):
    l = line.strip().split(':')
    _min = float(l[4].split(',')[0])
    _median = float(l[5].split(',')[0])
    _mean = float(l[6].split(',')[0])
    _max = float(l[7].split(',')[0])
    _std = float(l[8])
    return [_min, _median, _mean, _max, _std]

  f = open(file_path, 'r')
  content = f.readlines()
  pattern = 'Overall MAE:'
  for line in content:
    if pattern in line:
      overall = line.split(pattern)[-1].strip()
      overall = float(overall)
    elif 'Bin' in line:
      _bin = parse_bin_line(line)
      if 'Bin 0' in line:
        bin0 = _bin
      elif 'Bin 1' in line:
        bin1 = _bin
      elif 'Bin 2' in line:
        bin2 = _bin
      elif 'Bin 3' in line:
        bin3 = _bin
      elif 'Bin 4' in line:
        bin4 = _bin


  return overall, bin0, bin1, bin2, bin3, bin4


# check if recall was already computed
if os.path.isfile(DEFAULT_OUTPUT_FILE):
  with open(DEFAULT_OUTPUT_FILE, 'rb') as f:
    losses = pickle.load(f)
else:
  for loss_type in LOSS_TYPES:
    _overall = []
    _models = []
    _bins0 = []
    _bins1 = []
    _bins2 = []
    _bins3 = []
    _bins4 = []
    log_files = glob.glob('./models/train*_Loss-' + loss_type + '.config/train*.log')
    for log_file in log_files:
      overall, bin0, bin1, bin2, bin3, bin4 = get_loss_and_binwise_loss_from_log_file(log_file)
      _overall.append(overall)
      _model = log_file.split('/')[2]
      _models.append(_model)
      _bins0.append(bin0)
      _bins1.append(bin1)
      _bins2.append(bin2)
      _bins3.append(bin3)
      _bins4.append(bin4)
      
    _overall = np.asarray(_overall)
    _models = np.asarray(_models)
    _bins0 = np.asarray(_bins0)
    _bins1 = np.asarray(_bins1)
    _bins2 = np.asarray(_bins2)
    _bins3 = np.asarray(_bins3)
    _bins4 = np.asarray(_bins4)
    sort_indices = np.argsort(_overall)

    _overall = _overall[sort_indices]
    _models = _models[sort_indices]
    _bins0 = _bins0[sort_indices]
    _bins1 = _bins1[sort_indices]
    _bins2 = _bins2[sort_indices]
    _bins3 = _bins3[sort_indices]
    _bins4 = _bins4[sort_indices]


    losses[loss_type] = {
      'overall': _overall,
      'models': _models,
      0: _bins0,
      1: _bins1,
      2: _bins2,
      3: _bins3,
      4: _bins4,
    }

  with open(DEFAULT_OUTPUT_FILE, 'wb') as outfile:
    pickle.dump(losses, outfile)


header = '{:18} {:16} {:16} {:8} {:16} {:16} {:16} {:16} {:16}'.format('Loss', 'Model Topology', 'MAE', 'Bin', 'Min', 'Median', 'Mean', 'Max', 'Deviation')
print(header)
print('=' * len(header))
# print(losses)
best_models = []
for loss_type in LOSS_TYPES:
  overalls = losses[loss_type]['overall']
  models = losses[loss_type]['models']
  bin0 = losses[loss_type][0]
  bin1 = losses[loss_type][1]
  bin2 = losses[loss_type][2]
  bin3 = losses[loss_type][3]
  bin4 = losses[loss_type][4]
  bins = [bin0, bin1, bin2, bin3, bin4]


  for i in range(1):
    # print(models)
    best_models.append(models[i])    
    model = models[i].split('_')[3]
    for b_idx, b in enumerate(bins):
      if b_idx == 0:
        print('{:<18} {:<16} {:<16.2f} {:<8d} {:<16.2f} {:<16.2f} {:<16.2f} {:<16.2f} {:<16.2f}'.format(loss_type, model, overalls[i], b_idx, b[i][0], b[i][1], b[i][2], b[i][3], b[i][4]))
      else:
        print('{:<18} {:<16} {:<16} {:<8d} {:<16.2f} {:<16.2f} {:<16.2f} {:<16.2f} {:<16.2f}'.format('', '', '', b_idx, b[i][0], b[i][1], b[i][2], b[i][3], b[i][4], model))
  
  print('=' * len(header))

to_latex = True

if to_latex:
  print('\\begin{tabular}{ l | l | l | l | l | l | l | l | l}')
  print('\multirow[t]{2}{*}{\\bfseries Loss} & \\bfseries \gls{nn} Topology & \multirow[t]{2}{*}{\\bfseries MAE [mm]} &\\bfseries bin & \multicolumn{5}{|c}{\\bfseries Bin-wise Thickness Deviations [mm]}\\\\\cline{5-9}')
  print('                                    &                              &                                         &               & \\bfseries min & \\bfseries median & \\bfseries mean & \\bfseries max & \\bfseries std\\\\\hline\hline')
  for loss_type in LOSS_TYPES:
    overalls = losses[loss_type]['overall']
    models = losses[loss_type]['models']
    bin0 = losses[loss_type][0]
    bin1 = losses[loss_type][1]
    bin2 = losses[loss_type][2]
    bin3 = losses[loss_type][3]
    bin4 = losses[loss_type][4]
    bins = [bin0, bin1, bin2, bin3, bin4]


    for i in range(1):
      model = models[i].split('_')[3]
      for b_idx, b in enumerate(bins):
        if b_idx == 0:
          print('{:<18} & {:<16} & {:<16.2f} & {:<8d} & {:<16.2f} & {:<16.2f} & {:<16.2f} & {:<16.2f} & {:<16.2f}\\\\\cline{{4-9}}'.format(loss_type, model, overalls[i], b_idx, b[i][0], b[i][1], b[i][2], b[i][3], b[i][4]))
        else:
          print('{:<18} & {:<16} & {:<16} & {:<8d} & {:<16.2f} & {:<16.2f} & {:<16.2f} & {:<16.2f} & {:<16.2f}\\\\\cline{{4-9}}'.format('', '', '', b_idx, b[i][0], b[i][1], b[i][2], b[i][3], b[i][4], model))
  print('\end{tabular}')
  

for m in best_models:
  print(m)

# def set_box_color(bp, color):
#     plt.setp(bp['boxes'], color=color)
#     plt.setp(bp['whiskers'], color=color)
#     plt.setp(bp['whiskers'], linestyle='--', linewidth=1)
#     plt.setp(bp['caps'], color=color)
#     plt.setp(bp['medians'], color=color)

# # prepare data for plotting
# none_recalls = []
# zero_one_recalls = []
# mean_recalls = []
# mean_var_recalls = []

# for i, data_type in enumerate(DATA_TYPES):  
#   for j, norm in enumerate(NORM_TYPES):
#     _r = recalls[data_type][norm]['recalls']
#     if norm == NORM_TYPES[0]:
#       none_recalls.append(_r)
#     elif norm == NORM_TYPES[1]:
#       zero_one_recalls.append(_r)
#     elif norm == NORM_TYPES[2]:
#       mean_recalls.append(_r)
#     elif norm == NORM_TYPES[3]:
#       mean_var_recalls.append(_r)



# plt.figure()
# num_recalls = len(none_recalls)
# bpl0 = plt.boxplot(none_recalls, positions=np.array(range(num_recalls))*2.0-0.4, widths=0.2)
# set_box_color(bpl0, 'red')
# bpl1 = plt.boxplot(zero_one_recalls, positions=np.array(range(num_recalls))*2.0-0.1,widths=0.2)
# set_box_color(bpl1, 'green')
# bpl2 = plt.boxplot(mean_recalls, positions=np.array(range(num_recalls))*2.0+0.2, widths=0.2)
# set_box_color(bpl2, 'blue')
# bpl3 = plt.boxplot(mean_var_recalls, positions=np.array(range(num_recalls))*2.0+0.5, widths=0.2)
# set_box_color(bpl3, 'gray')

# fs = 16
# plt.rcParams.update({'font.size': fs})
# plt.plot([], c='red', label='None')
# plt.plot([], c='green', label='Zero-One')
# plt.plot([], c='blue', label='Mean')
# plt.plot([], c='gray', label='Mean-Variance')
# plt.legend()

# plt.ylabel('Recall/%', fontsize=fs)
# plt.xticks(range(0, len(DATA_TYPES) * len(NORM_TYPES), 2), DATA_TYPES, fontsize=fs)
# # plt.xlim(-2, len(DATA_TYPES) * 2)

# plt.xlim(-0.75,  len(DATA_TYPES) * 1.75)
# plt.ylim(0, 90)
# plt.tight_layout()
# # plt.title(classification_type_to_title[classification_type])
# plt.show()




###### Try pandas
# only_recalls = {}
# num_recalls = 0
# for data_type in DATA_TYPES:
#   only_recalls[data_type] = {}
#   for norm in NORM_TYPES:
#     _r = recalls[data_type][norm]['recalls']
#     only_recalls[data_type][norm] = _r


# norms = 25* [NORM_TYPES[0]] + 25 * [NORM_TYPES[1]] + 25 * [NORM_TYPES[2]] + 25 * [NORM_TYPES[3]]
# print(norms)
# print(len(norms))
# ah = [*only_recalls['auto_histogram']['None'], *only_recalls['auto_histogram']['zero-one'], *only_recalls['auto_histogram']['mean'], *only_recalls['auto_histogram']['mean-var']]
# fs = [*only_recalls['full_scale']['None'], *only_recalls['full_scale']['zero-one'], *only_recalls['full_scale']['mean'], *only_recalls['full_scale']['mean-var']]
# phase = [*only_recalls['phase']['None'], *only_recalls['phase']['zero-one'], *only_recalls['phase']['mean'], *only_recalls['phase']['mean-var']]
# print(len(ah))
# # ah = np.array(only_recalls['auto_histogram']['None'] + only_recalls['auto_histogram']['zero-one'] + only_recalls['auto_histogram']['mean'] + only_recalls['auto_histogram']['mean-var']).reshape(-1)
# # print(len(ah))
# df = pd.DataFrame({'Norm': norms, 'auto_histogram': ah, 'full_scale': fs, 'phase': phase})

# df.boxplot(by='Norm')
# # print(df)
# # dd = pd.melt(df, id_vars=['Norm'], value_vars=DATA_TYPES, var_name='Recall/%')
# # sns.boxplot(x='Norm', y='value', data=dd)



# # NUM_RECALLS = len(only_recalls[data_type][norm])
# # print(NUM_RECALLS)
# # df = pd.DataFrame.from_dict(only_recalls)
# # # df['GROUP'] = pd.Series(l * ['A')]
# # for idx, data_type in enumerate(DATA_TYPES):
# #   df['Group'] = pd.Series(NUM_RECALLS * [idx])


# # print(df)



# # print(df['auto_histogram'])
# # df.boxplot(column=DATA_TYPES)


# # df.boxplot(column=NORM_TYPES[1], by=DATA_TYPES)



# plt.show()
