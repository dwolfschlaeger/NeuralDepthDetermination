import os
import inspect
import sys
import numpy as np
import matplotlib.pyplot as plt

CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)

from Dataset import Dataset
import global_settings as gs




data_base_path = 'data/20-02-09_17_specimens_'
auto_histogram = 'auto_histogram'
full_scale = 'full_scale'
phase = 'phase'

dataset_params = {
    'file_path'         : data_base_path + phase + '.pickle',
    # 'input_shape'       : args.input_shape,
  }
# DATASET_TYPE = args.dataset_type

# load dataset
dataset = Dataset()
# if isinstance(dataset_params['input_shape'], list) and len(dataset_params['input_shape']) == 1:
#   dataset_params['input_shape'] = dataset_params['input_shape'][0]
dataset.init_dataset_from_file(**dataset_params)

x_start = 100
x_delta = 1
x_end = x_start + x_delta

spc_idx = 1
thickness = dataset.Y[spc_idx, :, x_start:x_end].reshape(-1)
# thickness = np.around(thickness, decimals=3)

sort_idx = np.argsort(thickness)
thickness = thickness[sort_idx]
_, unique_idx = np.unique(thickness, return_index=True)
thickness = thickness[unique_idx]
xt = np.arange(0, thickness.max(), 0.01)


EXC_FREQS = sorted([0.5, 0.4, 0.3, 0.2, 0.15, 0.125, 0.1, 0.09, 0.08, 0.07, 0.06, 0.05, 0.045, 0.04, 0.035, 0.03, 0.0275,
                    0.025, 0.0225, 0.02, 0.005], reverse=True)
# NEEDED TO MAP THE IDX OF A RAW PHASE IMAGE TO THE EXCITATION FREQUENCY
IDX_TO_EXC_FREQ = {idx + 1: EXC_FREQS[idx] for idx in range(len(EXC_FREQS))}

deg = 7
# symbols = [',', '-', '--']
symbols = ['-', '--']
symbol_idx = 0
for exc_freq in range(0, 21, 3):
  phase_img = dataset.X[spc_idx, :, x_start:x_end, exc_freq].reshape(-1)
  phase = phase_img[sort_idx]
  phase = phase[unique_idx]
  z = np.polyfit(thickness, phase, deg=deg)
  f = np.poly1d(z)
  # plt.plot(thickness, f(thickness), symbols[symbol_idx], label='%s Hz' % gs.IDX_TO_EXC_FREQ[exc_freq + 1])
  plt.plot(xt, f(xt), symbols[symbol_idx], linewidth=1, label='%s Hz' % IDX_TO_EXC_FREQ[exc_freq + 1])
  # plt.plot(xt, f(xt), label='%s Hz' % gs.IDX_TO_EXC_FREQ[exc_freq + 1])
  if (exc_freq + 1) % 9 == 0:
    symbol_idx += 1


plt.xlabel('thickness/mm')
plt.ylabel('phase/°')
plt.legend(bbox_to_anchor=(1, 1.02))
plt.grid()
plt.savefig('thickness_against_frequency_pf_degree_%s.svg' % deg, bbox_inches='tight')
plt.close()
