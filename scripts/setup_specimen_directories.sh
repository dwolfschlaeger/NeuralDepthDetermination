#!/usr/bin/env bash
# Create a directory for each specimen containing the sub directories

specimens='111 112 113 114 122 123 124 132 134'
suffixes='a b'
subdirs='auto_histogram auto_histogram_with_scale full_scale CT'

for specimen in $specimens;
do
  for suffix in $suffixes;
  do
    for subdir in $subdirs;
    do
      mkdir -p "$specimen$suffix/$subdir"
    done
  done
done
