import os
import inspect
import sys

CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)
from Config import ConfigGenerator
from Train import Train

dataset_prefix = 'data/16-12-19_17_specimens'

# second argument can be the dataset_prefix
if len(sys.argv) == 2:
  dataset_prefix = sys.argv[1]


def generate_configs():
  config_generator = ConfigGenerator('models/train_FF_norm_investigation', 'models', 'configs')
  configs = config_generator.generate_train_configs(dataset_prefix, network_types=['ff'], input_shapes=[21],
                                                    dropout_rates=[None], test_split=0.25,
                                                    val_split=0.25, num_layers=[4, 8],
                                                    num_units=[124, 256, 512, 1024])

  for config in configs:
    filename = config.value('filename', '')
    data_path = config.value('data_path', '')
    print('Running config: %s\n for data_path: %s' % (filename, data_path))
    Train(config)


def normalization_and_image_type_inspection():
  """
  Generates configs for normalization and image type inspection
  :return:
  """
  config_generator = ConfigGenerator('models/normalization_image_type_inspection_small_fixed_val', 'models', 'configs')
  shuffle_specimens = [0, 3, 4, 5, 7, 9, 10, 12, 14, 15, 6, 8, 13, 1, 2, 11, 16]
  configs = config_generator.generate_train_configs(dataset_prefix, dropout_rates=[None], test_split=0.25,
                                                    val_split=0.25, num_layers=[2, 4, 8],
                                                    num_units=[16, 32, 64, 128, 256, 512, 1024], shuffle_specimens=shuffle_specimens)


normalization_and_image_type_inspection()
