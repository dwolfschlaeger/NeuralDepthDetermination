#!/usr/local_rwth/bin/zsh

# ask for 10 GB memory
#SBATCH --mem-per-cpu=10240M   #M is the default and can therefore be omitted, but could also be K(ilo)|G(iga)|T(era)
# name the job
##SBATCH --job-name=SERIAL_JOB
#SBATCH --job-name=train_mlp_abstract.py
# declare the merged STDOUT/STDERR file
#SBATCH --output=output.%J.txt
# ask for 1 gpu
##SBATCH --gres=gpu:pascal:1
#SBATCH --time=5-00:00:00
#SBATCH --mail-user=kk732748@rwth-aachen.de
 
### begin of executable commands
date;hostname;pwd
#module load cuda/100 cudnn/7.4 

/home/kk732748/venvs/3.6/bin/python train_mlp.py


date
