#!/usr/bin/env python
import sys
import numpy as np
import matplotlib.pyplot as plt


assert len(sys.argv) == 2, 'Please provide a text file as a second argument'
file_name = sys.argv[1]
f = open(file_name, 'r')
content = f.readlines()
# print(content)
models = []
recalls = []
accs = []
max_length = 0
acc_idx = 4
recall_idx = -1

for row in content:
  _row = row.split(':')
  model = _row[0]
  recall = float(_row[recall_idx].strip())
  acc = float(_row[acc_idx].split(',')[0].strip())
  # print(recall, acc)
  models.append(model)
  accs.append(acc)
  recalls.append(recall)
  if len(model) > max_length:
    max_length = len(model)

print('Number of hits: %s' % len(models))
models = np.asarray(models)
accs = np.asarray(accs)
recalls = np.asarray(recalls)
print('Average recall', np.mean(recalls))
print('Recall Std', np.std(recalls))
print('Average acc', np.mean(acc))
plt.boxplot(recalls)
plt.show()
sort_indices = np.argsort(recalls)
recalls = recalls[sort_indices]
models = models[sort_indices]
accs = accs[sort_indices]

for i in range(len(models)):
  # print(models[i], accs[i])
  print(('{:' + str(max_length) + '} {:7.4f} {:7.4f}').format(models[i], recalls[i], accs[i]))

