# TASK WHICH WILL BE EXECUTED.
## POSSIBLE VALUES: label, train, evaluate, serve
## DEFAULT: 'train'
task = 'train'

# TYPE OF THE INPUT DATA, WHICH AT THE SAME TIME IS A SUB FOLDER IN THE SPECIMEN DIRECTORY: auto_histogram, full_scale, phase
image_type = 'auto_histogram'

# FILE WHERE ALL THE DATA IS CONTAINED
## DEFAULT: None
img_type = 'auto_histogram'
data_path = './data/24-11-19_data_%s.pickle' % img_type

# SHUFFLE OPTIONS FOR SPECIMEN ORDER
## POSSIBLE VALUES: 'random', list with indices which represent the specimens, None
## DEFAULT: 'random'
shuffle_specimens = [0, 2, 1]

# WHETHER TO SHUFFLE PIXELS FOR EACH SPECIMEN
## Default: False
shuffle_pixels = False

# NORMALIZATION TYPES FOR THE INPUT DATA
## POSSIBLE VALUES: min-max, mean-variance, mean or None
## DEFAULT: None
normalization = 'min-max'

# WHETHER TO USE DATA AUGMENTATION
## DEFAULT: False
use_data_augmentation = True

# TRAINING PARAMETERS
## DEFAULT: 0.3
test_split = 0.3
# DEFAULT: 0.3
val_split = 0.3
# DEFAULT: 100
num_epochs = 500
# DEFAULT: 'mae'
loss = 'mae'
# DEFAULT: 'nadam'
optimizer = 'nadam'
# DEFAULT: ['mae', 'accuracy']
metrics = ['mae', 'accuracy']

# input_shape = (512, 512, 21,)
# SHAPE OF THE INPUT LAYER. SHOULD CORRESPOND TO NUMBER OF EXCITATION FREQUENCY (C) FOR PIXEL-WISE TRAINING OR TO THE
# IMAGE SHAPE FOR IMAGE-WISE TRAINING (H, W, C)
## DEFAULT: (21,)
# input_shape = (21,1)
input_shape = (512, 512, 21)
# input_shape = (512, 512)

# SHAPE OF THE OUTPUT LAYER. SHOULD BE 1 FOR PIXEL-WISE TRAINING AND (H, W) FOR IMAGE-WISE TRAINING
## DEFAULT: 1
output_shape = 1
# output_shape = (512, 512)
## DEFAULT: 32
batch_size   = 64

# NETWORK TOPOLOGY
network      = {
  'h1':     {'class': 'Conv2D', 'filters': 30, 'kernel_size': 3, 'padding': 'same'},
  'output': {'class': 'Dense', 'units': output_shape, 'activation': 'relu', 'from': 'h1'}
}

# EARLY STOPPING PARAMETERS
## DEFAULT: True
use_early_stopping = True
## DEFAULT: 5
stopping_patience = 10
## DEFAULT: 'auto'
stopping_mode = 'min'

# OUTPUT DIRECTORY
## DEFAULT: 'models/filename'
output_dir = 'models/demo-train_CNN.config_%s-%s' % (normalization, img_type)
