# TASK WHICH WILL BE EXECUTED: label, train, evaluate, serve
task = 'train'
filename = 'demo-train-FF-binary-classification.config'

# FILE WHERE ALL THE DATA IS CONTAINED
## DEFAULT: None
data_type = 'auto_histogram'
# data_path = './data/20-02-09_17_specimens_%s.pickle' % data_type
data_path = './data/20-02-09_3_specimens_%s.pickle' % data_type

# SHUFFLE OPTIONS FOR SPECIMEN ORDER
## POSSIBLE VALUES: 'random', list with indices which represent the specimens, None
## DEFAULT: 'random'
shuffle_specimens = 'random'

# WHETHER TO SHUFFLE PIXELS FOR EACH SPECIMEN
## Default: False
shuffle_pixels = False

# NORMALIZATION TYPES FOR THE INPUT DATA
## POSSIBLE VALUES: zero-one, mean-variance, mean or None
## DEFAULT: None
normalization = 'mean-var'

# TRAINING PARAMETERS
## DEFAULT: 0.3
test_split = 0.3
# DEFAULT: 0.3
val_split = 0.3
# DEFAULT: 100
num_epochs = 500
# DEFAULT: 'mae'
loss = 'sparse_categorical_crossentropy'

# DEFAULT: 'nadam'
optimizer = 'nadam'
# DEFAULT: ['mae', 'accuracy']
metrics = ['mae', 'accuracy']

# input_shape = (512, 512, 21,)
# SHAPE OF THE INPUT LAYER. SHOULD CORRESPOND TO NUMBER OF EXCITATION FREQUENCY (C) FOR PIXEL-WISE TRAINING OR TO THE
# IMAGE SHAPE FOR IMAGE-WISE TRAINING (H, W, C)
## POSSIBLE VALUES:
## DEFAULT: (21,)
input_shape = 21

## DEFAULT: 32
batch_size   = 2048

# EARLY STOPPING PARAMETERS
## DEFAULT: True
use_early_stopping = True
## DEFAULT: 5
stopping_patience = 10
## DEFAULT: 'auto'
stopping_mode = 'min'

# OUTPUT DIRECTORY
## DEFAULT: 'models/filename'
output_dir = 'models/%s' % filename

# A LIST OF CLASS TO THICKNESS PAIRS IN ASCENDING THICKNESS ORDER
# IF STRING OR NONE IS PROVIDED AND LOSS=BINARY_CROSSENTROPY LABELS ARE COMPUTED AUTOMATICALLY
class_to_thickness = 'binary'


# SHAPE OF THE OUTPUT LAYER. SHOULD BE 1 FOR PIXEL-WISE TRAINING AND (H, W) FOR IMAGE-WISE TRAINING
## DEFAULT: 1
output_shape = 1
activation = 'relu'
if 'crossentropy' in loss:
  # binary classification
  output_shape = 2
  activation = 'softmax'
  if isinstance(class_to_thickness, (list, tuple)):
    output_shape = len(class_to_thickness)

# NETWORK TOPOLOGY
network      = {
  'h1':     {'class': 'Dense', 'units': 100,          'activation': 'tanh'},
  'h2':     {'class': 'Dense', 'units': 200,          'activation': 'relu'},
  'output': {'class': 'Dense', 'units': output_shape, 'activation': activation, 'from': 'h2'}
}
