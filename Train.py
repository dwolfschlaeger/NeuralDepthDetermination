import logging
import os
import random
import pickle
import copy

import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger, TensorBoard
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers import *

import global_settings as gs
from Dataset import Dataset
from utils import plot_utils
from Log import init_logger

seed = 1
tf.compat.v1.set_random_seed(seed)
random.seed(seed)
np.random.seed(seed)
logger = logging.getLogger(gs.LOGGER_NAME)


class Train:
  def __init__(self, config):
    """
    Loads the dataset, builds and trains a neural model.
    :param Config.Config config:
    """
    init_logger(config)
    self.dataset = Dataset()
    self.dataset.init_dataset_from_config(config)

    self.config = config
    self.output_dir = config.value('output_dir', os.path.join('models', config.filename))
    self.plot_dir = os.path.join(self.output_dir, 'plots')
    self.metrics = config.value('metrics', ['mae', 'accuracy'])
    self.input_shape = config.value('input_shape', gs.INPUT_SHAPE)
    self.loss = self.set_loss(config.value('loss', 'mae'))
    self.lr = config.value('learning_rate', None)
    str_opt = config.value('optimizer', 'nadam')
    self.optimizer = self.set_optimizer(str_opt, self.lr)

    self.batch_size = config.value('batch_size', 512)
    self.num_epochs = config.value('num_epochs', 100)
    self.use_early_stopping = config.value('use_early_stopping', True)
    self.stopping_patience = config.value('stopping_patience', 5)
    self.stopping_mode = config.value('stopping_mode', 'auto')
    self.model = None
    network_description = config.value('network', None)
    self.build_model(network_description)
    self.train_model()
    self.evaluate_model()

  @staticmethod
  def set_loss(loss):
    """
    Set loss based on the passed string
    :param str loss:
    :return:
    """
    # return huber loss
    if loss.lower() == 'huber':
      return tf.losses.Huber()
    return loss

  @staticmethod
  def set_optimizer(optimizer, lr=None):
    """

    :param str optimizer:
    :param None|float lr:
    :return:
    """
    opt = tf.optimizers.get(optimizer)
    if lr is not None:
      opt.learning_rate = lr
      # opt._set_hyper()
    return opt

  def build_model(self, network_description):
    """
    Builds a sequential model from the given network description
    :param list[dict] network_description:
    :return:
    """
    assert network_description is not None, 'Network description is None'
    assert isinstance(network_description, dict) and len(network_description.keys()) > 0, 'Invalid network description'
    assert self.input_shape is not None, 'input_shape has to be defined'
    network_desc_copy = copy.deepcopy(network_description)
    input_layer = tf.keras.Input(shape=self.input_shape, name='data')
    inputs = [input_layer]

    # instantiate layers from network description
    layer_name_to_layer = {}
    for layer_name, layer in network_desc_copy.items():
      layer.pop('from', None)
      layer_class = layer.pop('class')
      layer['name'] = layer_name
      created_layer = eval(layer_class)(**layer)
      layer_name_to_layer[layer_name] = created_layer

    # get all output layers
    output_names = []
    for layer_name, layer in network_description.items():
      if 'output' in layer_name.lower():
        output_names.append(layer_name)

    # gather layer blocks
    layer_blocks = []
    for output_name in output_names:
      layer_block = self.gather_layer_block(output_name, network_description)
      layer_blocks.append(layer_block)

    # create outputs
    outputs = []
    for layer_block in layer_blocks:
      if len(layer_block) > 0:
        x = input_layer
        for layer_name in layer_block:
          x = layer_name_to_layer[layer_name](x)
        outputs.append(x)

    self.model = tf.keras.Model(inputs, outputs, name=self.config.filename)
    self.reshape_targets(outputs)
    self.model.compile(self.optimizer, self.loss, self.metrics)
    self.write_num_model_params_to_file()
    logger.info(self.model.summary())

  def reshape_targets(self, outputs):
    assert isinstance(outputs, (list, tuple)) and len(outputs) > 0
    output_layer = outputs[0]
    # image wise input and
    if isinstance(self.input_shape, (list, tuple)) and len(self.input_shape) == 3:
      output_shape = output_layer.shape[-1]

      if self.dataset.Y_train is not None:
        N = self.dataset.Y_train.shape[0]
        self.dataset.Y_train = self.dataset.Y_train.reshape(N, output_shape)
      # if self.dataset.Y_train_labels is not None:
      #   N = self.dataset.Y_train_labels.shape[0]
      #   self.dataset.Y_train_labels = self.dataset.Y_train_labels.reshape(N, output_shape)
      if self.dataset.Y_val is not None:
        N = self.dataset.Y_val.shape[0]
        self.dataset.Y_val = self.dataset.Y_val.reshape(N, output_shape)
      # if self.dataset.Y_val_labels is not None:
      #   N = self.dataset.Y_val_labels.shape[0]
      #   self.dataset.Y_val_labels = self.dataset.Y_val_labels.reshape(N, output_shape)
      if self.dataset.Y_test is not None:
        N = self.dataset.Y_test.shape[0]
        self.dataset.Y_test = self.dataset.Y_test.reshape(N, output_shape)
      # if self.dataset.Y_test_labels is not None:
      #   N = self.dataset.Y_test_labels.shape[0]
      #   self.dataset.Y_test_labels = self.dataset.Y_test_labels.reshape(N, output_shape)

  def gather_layer_block(self, layer_name, network_description, layer_block=None):
    """
    Gathers dependencies between layers from top to bottom (output to input)
    :param str layer_name: name of the succeeding layer
    :param list|None layer_block:
    :param dict[dict] network_description:
    :return: layer_block
    """
    if layer_block is None or len(layer_block) == 0:
      layer_block = [layer_name]
    _from = network_description[layer_name].get('from')
    if _from is not None:
      layer_block.append(_from)
      self.gather_layer_block(_from, network_description, layer_block)
    else:
      layer_block.reverse()
    return layer_block

  def train_model(self):
    """
    Trains the model.
    :return:
    """
    assert self.model is not None
    # create output dir
    if not os.path.isdir(self.output_dir):
      os.mkdir(self.output_dir)
    # create plot dir
    if not os.path.isdir(self.plot_dir):
      os.mkdir(self.plot_dir)
    best_model_path = os.path.join(self.output_dir, 'best_model.h5')
    loss_and_metrics_log_file = os.path.join(self.output_dir, 'loss_and_metrics.log')
    tb_log_dir = os.path.join(self.output_dir, 'tensorboard_log')

    # check whether model was already trained
    if self.is_already_trained() and os.path.isfile(best_model_path):
      logger.info('Model with the same configuration was already trained')
      logger.info('Loading weights from existing model')
      self.model.load_weights(best_model_path)
      return
    # prepare data for training
    x = self.dataset.X_train
    y = self.dataset.Y_train
    x_val = self.dataset.X_val
    y_val = self.dataset.Y_val
    # if self.loss == 'binary_crossentropy':
    #   y_new = np.zeros((y.size, 2))
    #   y_new[np.arange(y.size), y] = 1
    #   y = y_new.reshape((y.shape + (2,)))
    #
    #   y_val_new = np.zeros((y_val.size, 2))
    #   y_val_new[np.arange(y_val.size), y_val] = 1
    #   y_val = y_val_new.reshape((y_val.shape + (2,)))

    val_data = None if x_val is None else (x_val, y_val)
    # create dir for tensorboard
    if not os.path.exists(tb_log_dir):
      os.makedirs(tb_log_dir)
    # setup callbacks
    monitor_loss = 'loss' if val_data is None else 'val_loss'
    callbacks = [EarlyStopping(monitor=monitor_loss, patience=self.stopping_patience),
                 ModelCheckpoint(monitor=monitor_loss, filepath=best_model_path, save_best_only=True),
                 CSVLogger(filename=loss_and_metrics_log_file, append=True),
                 TensorBoard(log_dir=tb_log_dir, write_images=True)]
    # train model
    logger.info('Train model')
    hist = self.model.fit(x, y, self.batch_size, self.num_epochs, validation_data=val_data, callbacks=callbacks)
    # save model, model overview and plot the losses
    model_path = os.path.join(self.output_dir, 'last_model.h5')
    self.model.save(model_path)
    plot_model_path = os.path.join(self.output_dir, 'model_overview.png')
    plot_model(self.model, to_file=plot_model_path, show_shapes=True)
    plot_utils.plot_metrics_from_history(hist, self.plot_dir)
    # load weights of the best model
    self.model.load_weights(best_model_path)

    # TODO: export model for serving
    # export model for serving
    # export_path = os.path.join(self.output_dir, 'simple_save')
    #
    # builder = tf.saved_model.builder.SavedModelBuilder(export_path)
    # builder.add_meta_graph_and_variables(
    #   keras.backend.get_session(),
    #   [tf.saved_model.tag_constants.SERVING],
    #   signature_def_map={
    #     'predict_images': prediction_signature,
    #     signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: classification_signature,
    #   },
    #   main_op=tf.tables_initializer()
    # )
    #
    # builder.save()
    # tf.saved_model.simple_save(
    #   keras.backend.get_session(),
    #   model_path,
    #   inputs = {'input_image': self.model.input},
    #   outputs = {t.name : t for t in self.model.outputs}
    # )

  def evaluate_model(self):
    """
    Evaluates the model.
    :return:
    """
    logger.info('Evaluate model')
    x = self.dataset.X_test
    y = self.dataset.Y_test

    results = self.model.evaluate(x, y)
    logger.info('test loss, test acc: %s' % results)
    logger.info('Loss: %s' % results[0])
    # logger.info('Accuracy, %s' % results[-1])
    y_pred = self.model.predict(x)
    # deviaton without backgournd
    nonzero_idx = np.nonzero(y)
    nonzero_y = y[nonzero_idx].reshape(-1)
    nonzero_y_pred = y_pred[nonzero_idx].reshape(-1)
    abs_mean_dev_plates =  np.mean(np.abs(nonzero_y - nonzero_y_pred))
    logger.info('Absolute mean dev plates: %s' % abs_mean_dev_plates)
    if isinstance(self.loss, str) and 'crossentropy' in self.loss:
      y_pred = tf.argmax(y_pred, axis=1).numpy()
    y = y.reshape(-1, gs.H, gs.W)
    y_pred = y_pred.reshape(-1, gs.H, gs.W)
    assert y.shape == y_pred.shape
    # classification
    if isinstance(self.loss, str) and 'crossentropy' in self.loss:
      confusion_matrix = tf.math.confusion_matrix(y.reshape(-1), y_pred.reshape(-1))
      logger.info('Confusion matrix')
      logger.info(confusion_matrix)
      from sklearn.metrics import precision_recall_fscore_support as score
      precision, recall, f1, _ = score(y.reshape(-1), y_pred.reshape(-1))
      # the defect class has idx 1 for binary and for three-class
      logger.info('Accuracy: %s, Precision: %s, Recall: %s' % (results[-1], precision[1], recall[1]))
      plot_utils.plot_confusion_matrix(confusion_matrix, self.dataset.class_to_thickness, self.plot_dir)
      plot_utils.plot_all_classification_results(y, y_pred, self.dataset.class_to_thickness, self.plot_dir)
      for n, _ in enumerate(y):
        specimen_name = self.dataset.specimen_names_test[n]
        plot_utils.plot_classification_result(y[n], y_pred[n], gs, self.dataset.class_to_thickness,
                                              output_dir=self.plot_dir, prefix=specimen_name)

      # confusion_matrix_path = os.path.join(self.output_dir, 'confusion_matrix')
      # tf.io.write_file(confusion_matrix_path, confusion_matrix)
    # regression
    else:
      # TODO
      self.evaluate_mae_bin_wise(y, y_pred)
      plot_utils.plot_scatter_plot(y, y_pred, output_dir=self.plot_dir)
      plot_utils.plot_ground_truth_vs_prediction_flattened(y, y_pred, output_dir=self.plot_dir, slicing_step=5000)
      plot_utils.plot_ground_truth_and_prediction_histograms(y, y_pred, output_dir=self.plot_dir)
      plot_utils.plot_ground_truth_vs_prediction_all(y, y_pred, gs, self.plot_dir)
      # average depth deviations for each hole (6x5)
      mae = np.zeros((6, 5))
      me = np.zeros((6, 5))
      mape = np.zeros((6, 5))
      mpe = np.zeros((6, 5))
      rmse = np.zeros((6, 5))

      for n, _ in enumerate(y):
        specimen_name = self.dataset.specimen_names_test[n]
        plot_utils.plot_ground_truth_vs_prediction(y[n], y_pred[n], gs, output_dir=self.plot_dir, prefix=specimen_name)
        plot_utils.plot_errors_per_bin(y[n], y_pred[n], output_dir=self.plot_dir, prefix=specimen_name, metrics=['mae'])
        plot_utils.plot_ground_truth_vs_prediction_flattened(y[n], y_pred[n], output_dir=self.plot_dir, prefix=specimen_name)
        plot_utils.plot_ground_truth_and_prediction_histograms(y[n], y_pred[n], output_dir=self.plot_dir, prefix=specimen_name)
        _mpe = plot_utils.plot_boreholes_depth_deviation(y[n], y_pred[n], gs, output_dir=self.plot_dir,
                                                          prefix=specimen_name, neighbouring_pixel_radius=7)

        mpe += _mpe
        me += (_mpe / 100)
        _mape = np.abs(_mpe)
        mape += _mape
        mae += (_mape / 100)
        rmse += np.square(_mape/100)
        plot_utils.plot_boreholes_area_deviation(y[n], y_pred[n], gs, output_dir=self.plot_dir, prefix=specimen_name,
                                                 use_cheating=True)
        # plot_utils.plot_boreholes_area_deviation_by_convex_hull(target, y_pred, gs, output_dir=self.plot_dir, prefix=specimen_name)
      num_samples = y.shape[0]
      me = me / num_samples
      mape = mape / num_samples
      mae = mae / num_samples
      mpe = mpe / num_samples
      rmse = np.sqrt(rmse / num_samples)
      plot_utils.plot_average_boreholes_depth_deviations(mape, percentage=True, output_dir=self.plot_dir, suffix='mape')
      plot_utils.plot_average_boreholes_depth_deviations(mpe, percentage=True, output_dir=self.plot_dir, suffix='mpe')
      plot_utils.plot_average_boreholes_depth_deviations(mae, percentage=False, output_dir=self.plot_dir, suffix='mae')
      plot_utils.plot_average_boreholes_depth_deviations(me, percentage=False, output_dir=self.plot_dir, suffix='me')
      plot_utils.plot_average_boreholes_depth_deviations(rmse, percentage=False, output_dir=self.plot_dir, suffix='rmse')



  def is_already_trained(self):
    """
    Determines whether the model was already trained.
    :return: bool
    """
    conf_name = '.config'
    conf_path = os.path.join(self.output_dir, conf_name)
    if not os.path.isfile(conf_path):
      if not os.path.isdir(self.output_dir):
        os.mkdir(self.output_dir)
      f = open(conf_path, 'wb')
      pickle.dump(self.config, f)
      f.close()
      return False
    f = open(conf_path, 'rb')
    old_conf = pickle.load(f)
    f.close()
    # if old_conf != self.config:
    #   f = open(conf_path, 'wb')
    #   pickle.dump(self.config, f)
    #   f.close()
    #   return False
    return True

  def write_num_model_params_to_file(self):
    """
    Writes the number of parameters to a file.
    :return:
    """
    num_params = self.model.count_params()
    if not os.path.isdir(self.output_dir):
      # os.mkdir(self.output_dir)
      os.makedirs(self.output_dir)
    filename = os.path.join(self.output_dir, 'num_params.txt')
    f = open(filename, 'w')
    f.write(str(num_params))
    f.close()

  @staticmethod
  def model_name_from_network_description(network_description, bs, loss, optimizer):
    assert isinstance(network_description, list)
    network = copy.deepcopy(network_description)
    name = 'LOSS-%s_BS-%s_OPT-%s' % (loss, bs, optimizer)
    for layer in network:
      layer_class = layer.pop('class')
      params = list(layer.values())
      params = '-'.join([str(elem) for elem in params])
      name += '_%s-' % layer_class
      name += params

    return name

  def evaluate_mae_bin_wise(self, y, y_pred, bins=(0, 1, 2, 3, 4)):
    """
    Computes
    :param y:
    :param y_pred:
    :param bins:
    :return:
    """
    y = y.reshape(-1)
    y_pred = y_pred.reshape(-1)
    sort_indices = np.argsort(y)
    y = y[sort_indices]
    y_pred = y_pred[sort_indices]
    bins = list(set(bins))
    bins.sort()
    result = {b: [] for b in bins}
    # check for background
    abs_diff = np.absolute(y - y_pred)
    logger.info('Overall MAE: %s', np.mean(abs_diff))
    prev_bin = -1
    for b in bins:
      # zero bins
      if b == 0:
        bin_indices = np.argwhere(y == 0)
      # last bin (sound area)
      elif b == bins[-1]:
        bin_indices = np.argwhere(y > prev_bin)
      # defect area
      else:
        bin_indices = np.argwhere((y <= b) & (y > prev_bin))

      diffs = abs_diff[bin_indices]
      result[b] = diffs
      logger.info('Bin %s : Min: %s, Median: %s, Mean: %s, Max: %s, Std: %s'
                  % (b, diffs.min(), np.median(diffs), np.mean(diffs), diffs.max(), np.std(diffs)))
      prev_bin = b

    plot_utils.plot_binwise_boxplots(result, output_dir=self.plot_dir)
